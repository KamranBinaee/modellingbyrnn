'''
Created on Apr 9, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD, RMSprop, Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN
from keras.utils.np_utils import accuracy

import plotly
from plotly.graph_objs import Scatter, Layout
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from IPython.lib.tests.test_pretty import MyList

print(plotly.__version__)

plotly.offline.init_notebook_mode()


def calculateMSE(testData, modelData):

    print(testData.shape)
    print(modelData.shape)
    #mse = (np.sum(np.power(testData - modelData,2), axis = 0))/testData.shape[0]
    mse = (np.sum(np.abs(testData - modelData), axis=0)) / testData.shape[0]
    print('mse Shape', mse.shape)
    # print(mse)
    std = np.std(testData - modelData, axis=0)
    print('std Shape', std.shape)
    # print(std)

    return mse, std


def plotMSE(testOutputList, modelOutputList, timeStep):

    # print('Test',testOutputList)
    # print('Model',modelOutputList)

    meanMatrix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    stdMatrix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    for i in range(len(testOutputList)):
        #print('Shapes', testOutputList[i].shape, modelOutputList[i].shape)
        mean, std = calculateMSE(testOutputList[i], modelOutputList[i])
        meanMatrix[i, :] = mean
        stdMatrix[i, :] = std

    x = np.arange(len(testOutputList))
    x = x / 75.
    meanHandX = meanMatrix[:, 0]
    meanHandY = meanMatrix[:, 1]
    meanHandZ = meanMatrix[:, 2]

    stdHandX = stdMatrix[:, 0]
    stdHandY = stdMatrix[:, 1]
    stdHandZ = stdMatrix[:, 2]

    meanGazeX = meanMatrix[:, 7]
    meanGazeY = meanMatrix[:, 8]

    stdGazeX = stdMatrix[:, 7]
    stdGazeY = stdMatrix[:, 8]

    meanHeadX = meanMatrix[:, 9]
    meanHeadY = meanMatrix[:, 10]
    meanHeadZ = meanMatrix[:, 11]

    stdHeadX = stdMatrix[:, 9]
    stdHeadY = stdMatrix[:, 10]
    stdHeadZ = stdMatrix[:, 11]

    pd.to_pickle(meanMatrix,  r'.\Results\meanMatrix_referrence.pickle')
    pd.to_pickle(stdMatrix,  r'.\Results\stdMatrix_referrence.pickle')
    print('Hacked! Going to Quit')
    return
    plt.figure()
    plt.errorbar(x, meanHandX, yerr=stdHandX,
                 fmt='--bo', ecolor='b', label='Hand X')
    plt.errorbar(x + 0.1 / 75., meanHandY, yerr=stdHandY,
                 fmt='--ro', ecolor='r', label='Hand Y')
    plt.errorbar(x + 0.2 / 75., meanHandZ, yerr=stdHandZ,
                 fmt='--go', ecolor='g', label='Hand Z')
    plt.grid(True)
    plt.legend()
    plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Hand Position Error [m]')
    plt.savefig('HandPredictionError.png')
    plt.savefig('HandPredictionError.svg', format='svg', dpi=1000)

    plt.figure()
    plt.errorbar(x, meanGazeX, yerr=stdGazeX, fmt='--bo',
                 ecolor='b', label='Gaze Azimuth')
    plt.errorbar(x + 0.1 / 75., meanGazeY, yerr=stdGazeY,
                 fmt='--ro', ecolor='r', label='Gaze Elevation')
    plt.grid(True)
    plt.legend()
    plt.title("Model Gaze Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Gaze Position Error [degree]')
    plt.savefig('GazePredictionError.png')
    plt.savefig('GazePredictionError.svg', format='svg', dpi=1000)

    plt.figure()
    plt.errorbar(x, meanHeadX, yerr=stdHeadX,
                 fmt='--bo', ecolor='b', label='Head X')
    plt.errorbar(x + 0.1 / 75., meanHeadY, yerr=stdHeadY,
                 fmt='--ro', ecolor='r', label='Head Y')
    plt.errorbar(x + 0.2 / 75., meanHeadZ, yerr=stdHeadZ,
                 fmt='--go', ecolor='g', label='Head Z')
    plt.grid(True)
    plt.legend()
    plt.title("Model Head Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Head Position Error [m]')
    plt.savefig('HeadPredictionError.png')
    plt.savefig('HeadPredictionError.svg', format='svg', dpi=1000)


def testModel(model=None, rawDataSetFileName=None, testDataSetFileName=None, data=None, epochNumber=None):

    if testDataSetFileName is None:
        print('No Path Specified for Training Set!!\n ==> Quit Training\n\n')
        return None
    else:
        print('Reading Testing Data Set: ', testDataSetFileName)
        testDataSet = pd.read_pickle(testDataSetFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    #print('feature 1 ', testDataSet[0,:,5])
    [mean, std] = calculateStats(rawDataSet)
    #print('\nmean = ',mean)
    #print('\nstd = ',std)
    feat_mean = mean
    feat_std = std
    #feat_mean = np.array([ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737])
    #feat_std =  np.array([ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025])

    for index in range(len(feat_mean)):
        testDataSet[:, :, index] -= feat_mean[index]
        testDataSet[:, :, index] = testDataSet[:, :, index] / (feat_std[index])
    #print('feature 1 ', testDataSet[0,:,5],'\n')

    #testDataSet = normalizeDataSet(testDataSet, feat_mean,  feat_std)
    #testDataSet = testDataSet - np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0)
    #testDataSet = np.divide(testDataSet, np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0))
    dataSetDimension = testDataSet.shape
    print(dataSetDimension)

    #print (testDataSet['t1'][0].keys())
    #print (testDataSet['t1'][0].items())

    #training_Input = testDataSet[:,0:dataSetDimension[1],:]
    #training_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
    if timeStep == None:
        testing_Input = testDataSet[:, 0:dataSetDimension[1], :]
        testing_Output = testDataSet[:, dataSetDimension[1] - 1, 0:16]
        print("No Prediction Selected!")
    else:
        testing_Input = testDataSet[:, 0:45, :]
        testing_Output = testDataSet[:, 45 + timeStep, 0:16]
        print("TimeStep = ", str(45 + timeStep), " Selected!")

    #X_train, y_train, X_test, y_test = data
    #cb = EarlyStopping(monitor='val_loss', patience=7, verbose=0, mode='auto')
    #history = model.fit(training_Input, training_Output,batch_size=128, nb_epoch=epochNumber, callbacks = [cb], validation_split=0.15, shuffle=True)

    #print('Running Model Evaluation ...')
    #score, accuracy = model.evaluate(testing_Input, testing_Output, batch_size = 128, verbose = 0 )
    # print(accuracy)
    # print(score)
    print('Running Model Prediction ...')
    modelOutput = model.predict(testing_Input, verbose=0)
    print(modelOutput.shape)
    print(testing_Output.shape)
    #mse = mean_squared_error(testing_Output.T, modelOutput.T, multioutput='raw_values')
    # print(mse.shape)
    # print(min(mse))
    # print(max(mse))
    # print(np.std(mse))
    figureName = 'outputFigure_45_Input_' + str(timeStep) + '_Output'
    # Un-normalizing the Data
    for index in range(modelOutput.shape[1]):
        modelOutput[:, index] = modelOutput[:, index] * (feat_std[index])
        modelOutput[:, index] += feat_mean[index]
        testing_Output[:, index] = testing_Output[:, index] * (feat_std[index])
        testing_Output[:, index] += feat_mean[index]

    '''
    predicted = model.predict(X_test)
    predicted = np.array(predicted)
   
    mse_predict = mean_squared_error(predicted, y_test, multioutput='raw_values')
    
    for item in history.history['loss']:
        f.write('{}\n'.format(item))

    for item in history.history['val_loss']:
        f.write('{}\n'.format(item))
    
    
    '''

    return model, modelOutput, testing_Output


if __name__ == "__main__":
    rawDataSetFileName = r'.\Datasets\Success\DataSet_Formatted_Success.pickle'
    #dataSetFileName = None
    trialTypeFileName = r'.\Datasets\Success\trialType_Success.pickle'

    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_6.pickle'
    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_6.pickle'
    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_6.pickle'
    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_6.pickle'

    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_21.pickle'
    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_21.pickle'
    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_21.pickle'
    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_21.pickle'

    #inputSequenceList = [3, 6, 9, 12, 15, 18, 21]
    # windowSize = [15, 20, 25, 30, 35, 40, 45]#, 50, 55, 60, 65, 70, 75]#, 80]
    windowSize = [80]
    learningRateList = [0.0001]
    epochNumberList = [100]
    timeStepList = [1]  # np.arange(0,35)
    modelOutputList = []
    testOutputList = []
    print("TimeSteps = ", timeStepList)
    for timeStep in timeStepList:
        for epochNumber in epochNumberList:
            for learningRate in learningRateList:
                for inputSeqLength in windowSize:
                    print('\n\nTraining on Input Sequence Length = ', inputSeqLength)
                    print('Learning Rate = ', learningRate)
                    print('Epoch Number = ', epochNumber)
                    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_' + \
                        str(inputSeqLength) + '.pickle'
                    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_' + \
                        str(inputSeqLength) + '.pickle'
                    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_' + \
                        str(inputSeqLength) + '.pickle'
                    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_' + \
                        str(inputSeqLength) + '.pickle'
                    weights_path = r'.\Results\Weights\PerSubjectModel\2016-5-3-12-52\weights_' + \
                        str(inputSeqLength - 35) + '_' + \
                        str(timeStep) + '_0.0001_100'
                    model = createRNN(inputSeqLength - 35,
                                      weights_path, learningRate)
                    print('RNN-LSTM Model Created ...\n')
                    model, modelOutput, testingOutput = testModel(
                        model, rawDataSetFileName, testDataSetFileName, epochNumber=epochNumber)
                    #pd.to_pickle(modelOutput,  r'.\Results\modelOutput_' + str(inputSeqLength - 35) + '_' + str(timeStep) +'_0.0001_100.pickle')
                    modelOutputList.append(modelOutput)
                    testOutputList.append(testingOutput)
                    print('Testing Shape', testingOutput.shape)
                    print('Model Shape', modelOutput.shape)
                    #plotMSE(modelOutputList, modelOutputList, timeStep)

    plotMSE(testOutputList, modelOutputList, timeStep)
