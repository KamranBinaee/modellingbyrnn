'''
Created on Feb 4, 2017

@author: Kamran Binaee
'''
from models import LSTM_50_50_50_1prediction
import pandas as pd
import os
import numpy as np
from matplotlib import pyplot as plt
import copy

def plotMyData(data, label, title):
    plt.close()
    plt.figure()
    plt.plot(data, label = label)
    plt.title(title)
    plt.xlabel('timesteps')
    plt.grid(True)
    plt.legend()

    
def seq_prediciton(model, weights_path, test_path, in_seq_length):
    test = pd.read_pickle(test_path)
    feat_mean = np.array([[ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737]])
    feat_std =  np.array([[ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025]])

    testTrialList = list(test.keys())
    # data normalization
    for trialNumber in testTrialList:
        trial=test[trialNumber]
        trial = trial - np.repeat(feat_mean, trial.shape[0] , axis=0)
        trial = np.divide(trial, np.repeat(feat_mean, trial.shape[0] , axis=0))
        
        start_point = 0
        in_seq = copy.deepcopy(trial[start_point:start_point+in_seq_length,:])
        #print('input sequence values', in_seq[:,0])
        in_seq = in_seq.reshape([1,in_seq.shape[0],in_seq.shape[1]])
        
        #print('input trial 6 ', trial[0:6,0])
        
        model.load_weights(weights_path)
        
        new_values = np.zeros([1,10])
        predicted_seq = np.zeros([trial.shape[0]-start_point,5])
        predicted_seq[0:in_seq_length,:] = copy.deepcopy(trial[start_point:start_point+in_seq_length,:5])
    
        for i in range(trial.shape[0]-in_seq_length-start_point):
            prediction = model.predict(in_seq)
            print('prediction shape', prediction.shape)
            # put the predicted value into new_value vector with the vector that corresponds to this prediction 
            new_values[:,0:5] = prediction
            new_values[:,5:10] = copy.deepcopy(trial[in_seq_length+i,5:10])
            print("prediction")
            in_seq[:,0:-1, :] = in_seq [:,1:,:]
            in_seq[:,in_seq.shape[1]-1,:] = new_values
            predicted_seq[in_seq_length+i,:] = prediction 
            
        print(predicted_seq.shape)
        titles = ['Hand_X','Hand_Y','Hand_Z', 'Gaze_El', 'Gaze_Az']
        for i in range(prediction.shape[1]):
            plt.figure(i)
            plt.plot(range(len(trial[start_point:,i])), trial[start_point:,i],'bo', label = titles[i] + ' Human')
            plt.plot(range(len(trial[start_point:,i])), predicted_seq[:,i],'go' , label = titles[i] + ' Model')
            plt.title(titles[i])
            plt.xlabel('Sample')
            plt.grid(True)
            plt.legend(loc = (0.65, 1))
            plt.savefig('.\Results\LSTM_50_50_50_adam_0.01\Seq_prediction\{}_input_seq{}_start{}_feature{}.png'.format(str(trialNumber),in_seq_length,start_point,str(i)))
            plt.close()
        #plt.show()
if __name__=="__main__":
    weights_dir = './Results/LSTM_50_50_50_adam_0.01/Weights/Normalized'
    for filename in os.listdir(weights_dir):
        weights_path = '{}/{}'.format(weights_dir,filename)
        # here I'm extracting an input sequence length from weights filename
        weights_filename = os.path.basename(weights_path)
        weights_filename_root,_ = os.path.splitext(weights_filename)
        in_seq_length = int(weights_filename_root[-2:])-1
        print('input sequence length', in_seq_length)
        
        model = LSTM_50_50_50_1prediction(in_seq_length,weights_path)
        
        test_path = '.\Datasets\Success\Test_DataSet_Success_ByTrial.pickle'
        
        seq_prediciton(model, weights_path, test_path, in_seq_length)
    #plt.show()
        
        
    