'''
Created on Mar 10, 2017

@author: Kamran Binaee
'''
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
#from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
import pickle
import _pickle as cPickle

#from keras.backend.tensorflow_backend import dtype

rawDataSetFileName = r'.\Datasets\Success\DataSet_Formatted_Success.pickle'
dataSetFileName = rawDataSetFileName
trialTypeFileName = r'.\Datasets\Success\trialType_Success.pickle'

# trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_6.pickle'
# testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_6.pickle'
# trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_6.pickle'
# trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_6.pickle'
# 
# trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_21.pickle'
# testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_21.pickle'
# trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_21.pickle'
# trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_21.pickle'

print('Running ChangePickleFileFormat.py ... \n')

fileName = r'D:\Kamran Backup\Backup Before Windows Change\NewRNNproject\Datasets\Success\Test_DataSet_Success_80.pickle'
tempData = pd.read_pickle(fileName)
#df.to_pickle('RL_Data.pkl')
with open('testDataSet.pkl', 'wb') as pickle_file:
    cPickle.dump(tempData, pickle_file, protocol=2)
    pickle_file.close()
print('TestDataSize = ', tempData[:,44,:].shape)

modelOffsetList = [0,5,10,15,20,25,30]
filePath = r'D:\Kamran Backup\Backup Before Windows Change\NewRNNproject\Results\ModelOutput\41_Feat_45_Input_35_Output_50_LSTM'
modelOutput = list()
for i in modelOffsetList:
    tempData = pd.read_pickle(filePath+'\modelOutput_45_'+str(i)+'_0.0001_100.pickle')
    modelOutput.append(tempData)
    print('OutDataSize = ', tempData.shape)
    with open('modelOutput'+str(i)+'.pkl', 'wb') as pickle_file:
        cPickle.dump(tempData, pickle_file, protocol=2)
        pickle_file.close()

print('Number of Model Outputs = ', len(modelOutput))
