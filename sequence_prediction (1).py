'''
Created on Feb 4, 2017

@author: Kamran Binaee
'''
from models import LSTM_50_50_50_1prediction
import pandas as pd
import os
import numpy as np
from matplotlib import pyplot as plt
import copy

def plotMyData(data, label, title):
    plt.close()
    plt.figure()
    plt.plot(data, label = label)
    plt.title(title)
    plt.xlabel('timesteps')
    plt.grid(True)
    plt.legend()

    
def seq_prediciton(model, weights_path, test_path, in_seq_length):
    test = pd.read_pickle(test_path)
    trial=test['trial729']

    start_point = 0
    in_seq = copy.deepcopy(trial[start_point:start_point+in_seq_length,:])
    print('input sequence values', in_seq[:,0])
    in_seq = in_seq.reshape([1,in_seq.shape[0],in_seq.shape[1]])
    
    
    model.load_weights(weights_path)
    
    new_values = np.zeros([1,10])
    predicted_seq = np.zeros([trial.shape[0]-start_point,5])
    predicted_seq[0:in_seq_length,:] = copy.deepcopy(trial[start_point:start_point+in_seq_length,:5])
    
    for i in range(trial.shape[0]-in_seq_length-start_point):
        prediction = model.predict(in_seq)
        print('prediction shape', prediction.shape)
        # put the predicted value into new_value vector with the vector that corresponds to this prediction 
        new_values[:,0:5] = prediction
        new_values[:,5:10] = copy.deepcopy(trial[in_seq_length+i,5:10])
        print("prediction")
        in_seq[:,0:-1, :] = in_seq [:,1:,:]
        in_seq[:,in_seq.shape[1]-1,:] = new_values
        predicted_seq[in_seq_length+i,:] = prediction 
        
        
    titles = ['hand_x','hand_y','hand_z', 'gaze_x', 'gaze_y']
    for i in range(prediction.shape[1]):
        plt.figure()
        plt.plot(range(len(trial[start_point:,i])), trial[start_point:,i],'bo', label = titles[i] + ' Human')
        plt.plot(range(len(trial[start_point:,i])), predicted_seq[:,i],'go' , label = titles[i] + ' Model')
        plt.title(titles[i])
        plt.xlabel('timesteps')
        plt.grid(True)
        plt.legend()

if __name__=="__main__":
    
    weights_path = 'weights_505050_adam_inseq06'
    # here I'm extracting an input sequence length from weights filename
    weights_filename = os.path.basename(weights_path)
    weights_filename_root,_ = os.path.splitext(weights_filename)
    in_seq_length = int(weights_filename_root[-2:])-1
    print('input sequence length', in_seq_length)
    
    model = LSTM_50_50_50_1prediction(in_seq_length,weights_path)
    
    test_path = '.\Datasets\Success\Test_DataSet_Success_ByTrial.pickle'
    
    seq_prediciton(model, weights_path, test_path, in_seq_length)
    plt.show()
        
        
    