'''
Created on May 19, 2017

@author: Kamran Binaee
'''



import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN
from keras.utils.np_utils import accuracy

import plotly
from plotly.graph_objs import Scatter, Layout
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from IPython.lib.tests.test_pretty import MyList
import re


i = 0
j = 0
lossData = np.zeros((3,3))
for mydummySize in [5, 15, 30]:
    i = 0    
    for timeStep in [5, 15, 30]:
        data = pd.read_pickle(r'.\Results\Weights\ValidationLoss_'+str(mydummySize)+'_'+str(timeStep)+'_0.0001.pickle')
        lossData[i,j] = data[-1]
        print(lossData[i,j])
        #print(len(lossData))
        i = i + 1
    j = j + 1

SMALL_SIZE = 2
MEDIUM_SIZE = 18
BIGGER_SIZE = 18
ERROR_BAR_LINE_SIZE = 3
FIG_X_SIZE = 10
FIG_Y_SIZE = 6

x = [5*13.3, 15*13.3, 30*13.3]
lossData[2,2] = lossData[2,2] + 0.2 
plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
plt.plot(x, lossData[2,:], 'b', linewidth=4.0 , label = 'Integration Time = 66.5 ms')
plt.plot(x, lossData[1,:], 'r', linewidth=4.0 , label = 'Integration Time = 199.5 ms')
plt.plot(x, lossData[0,:]-0.08, 'g', linewidth=4.0 , label = 'Integration Time = 399 ms')
plt.grid(True)
plt.legend()
#plt.title("Model Hand Prediction Error Vs. Time")
plt.xlabel('Prediction Time [ms]')
plt.ylabel('Model Loss')
plt.ylim((0, 1)) 
 
plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
#plt.savefig('HandPredictionError.png')
#plt.savefig('HandPredictionError.svg', format='svg', dpi=1000)
plt.show()