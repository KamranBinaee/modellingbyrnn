'''
Created on Mar 10, 2017

@author: Kamran Binaee
'''
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
#from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD, RMSprop, Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis

#from keras.backend.tensorflow_backend import dtype


def findTrialType(preBD, postBD):
    trialType = ''
    if(preBD == 0.6 and postBD == 0.3):
        trialType = 't1'
    elif(preBD == 0.6 and postBD == 0.4):
        trialType = 't2'
    elif(preBD == 0.6 and postBD == 0.5):
        trialType = 't3'
    elif(preBD == 0.8 and postBD == 0.3):
        trialType = 't4'
    elif(preBD == 0.8 and postBD == 0.4):
        trialType = 't5'
    elif(preBD == 0.8 and postBD == 0.5):
        trialType = 't6'
    elif(preBD == 1.0 and postBD == 0.3):
        trialType = 't7'
    elif(preBD == 1.0 and postBD == 0.4):
        trialType = 't8'
    elif(preBD == 1.0 and postBD == 0.5):
        trialType = 't9'
    else:
        print('Not valid Trial Type!!!')

    return trialType


def calculateNorm(row):
    return np.sqrt(row['X']**2 + row['Y']**2 + row['Z']**2)


def calculateBallSize(row):
    return


def calculateHeadVelocity(rawDataFrame, processedDataFrame):

    headPos_fr_XYZ = rawDataFrame['viewPos'].values
    headPos_fr_XYZ_shifted = np.roll(headPos_fr_XYZ, -1, axis=0)
    headQuat_fr_XYZW = rawDataFrame['viewQuat'].values
    headQuat_fr_XYZW_shifted = np.roll(headQuat_fr_XYZW, -1, axis=0)

    headPos_fr_XYZ = headPos_fr_XYZ_shifted - headPos_fr_XYZ
    headQuat_fr_XYZW = headQuat_fr_XYZW_shifted - headQuat_fr_XYZW

    timeArray = rawDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted - timeArray

    headVelocity_fr_XYZ = np.zeros((headPos_fr_XYZ.shape))
    headQuatVelocity_fr_XYZW = np.zeros((headQuat_fr_XYZW.shape))
    for i in range(3):
        headVelocity_fr_XYZ[:, i] = np.divide(headPos_fr_XYZ[:, i], timeDiff)
    for i in range(4):
        headQuatVelocity_fr_XYZW[:, i] = np.divide(
            headQuat_fr_XYZW[:, i], timeDiff)

    processedDataFrame.loc[:, ('headVelocity', 'X')
                           ] = headVelocity_fr_XYZ[:, 0]
    processedDataFrame.loc[:, ('headVelocity', 'Y')
                           ] = headVelocity_fr_XYZ[:, 1]
    processedDataFrame.loc[:, ('headVelocity', 'Z')
                           ] = headVelocity_fr_XYZ[:, 2]

    processedDataFrame.loc[:, ('headQuatVelocity', 'X')
                           ] = headQuatVelocity_fr_XYZW[:, 0]
    processedDataFrame.loc[:, ('headQuatVelocity', 'Y')
                           ] = headQuatVelocity_fr_XYZW[:, 1]
    processedDataFrame.loc[:, ('headQuatVelocity', 'Z')
                           ] = headQuatVelocity_fr_XYZW[:, 2]
    processedDataFrame.loc[:, ('headQuatVelocity', 'W')
                           ] = headQuatVelocity_fr_XYZW[:, 3]

    return processedDataFrame


def calculateGazeVelocity(rawDataFrame, processedDataFrame):

    eyeToScreenDistance = 0.0725
    x = processedDataFrame.gazePoint.X.values
    y = processedDataFrame.gazePoint.Y.values
    processedDataFrame.loc[:, ('gazePoint', 'X')] = (
        180 / np.pi) * np.arctan(x / eyeToScreenDistance)
    processedDataFrame.loc[:, ('gazePoint', 'Y')] = (
        180 / np.pi) * np.arctan(y / eyeToScreenDistance)
    gazeAngle_fr_XY = processedDataFrame['gazePoint'].values
    gazeAngle_fr_XY_shifted = np.roll(gazeAngle_fr_XY, -1, axis=0)
    gazeAngle_fr_XY = gazeAngle_fr_XY_shifted - gazeAngle_fr_XY

    timeArray = rawDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted - timeArray
    gazeVelocity_fr_XY = np.zeros((gazeAngle_fr_XY.shape))
    for i in range(2):
        gazeVelocity_fr_XY[:, i] = np.divide(gazeAngle_fr_XY[:, i], timeDiff)

    processedDataFrame.loc[:, ('gazeVelocity', 'X')] = gazeVelocity_fr_XY[:, 0]
    processedDataFrame.loc[:, ('gazeVelocity', 'Y')] = gazeVelocity_fr_XY[:, 1]

    return processedDataFrame


def calculateHandVelocity(rawDataFrame, processedDataFrame):

    handPosition_fr_XYZ = rawDataFrame.paddlePos.values
    handPosition_fr_XYZ_shifted = np.roll(handPosition_fr_XYZ, -1, axis=0)
    handPosition_fr_XYZ = handPosition_fr_XYZ_shifted - handPosition_fr_XYZ

    timeArray = rawDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted - timeArray

    handVelocity_fr_XYZ = np.zeros((handPosition_fr_XYZ.shape))
    for i in range(3):
        handVelocity_fr_XYZ[:, i] = np.divide(
            handPosition_fr_XYZ[:, i], timeDiff)

    processedDataFrame.loc[:, ('handVelocity', 'X')
                           ] = handVelocity_fr_XYZ[:, 0]
    processedDataFrame.loc[:, ('handVelocity', 'Y')
                           ] = handVelocity_fr_XYZ[:, 1]
    processedDataFrame.loc[:, ('handVelocity', 'Z')
                           ] = handVelocity_fr_XYZ[:, 2]

    handQuat_fr_XYZ = rawDataFrame.paddleQuat.values
    handQuat_fr_XYZ_shifted = np.roll(handQuat_fr_XYZ, -1, axis=0)
    handQuat_fr_XYZ = handQuat_fr_XYZ_shifted - handQuat_fr_XYZ

    handQuatVelocity_fr_XYZ = np.zeros((handQuat_fr_XYZ.shape))
    for i in range(4):
        handQuatVelocity_fr_XYZ[:, i] = np.divide(
            handQuat_fr_XYZ[:, i], timeDiff)

    processedDataFrame.loc[:, ('handQuatVelocity', 'X')
                           ] = handQuatVelocity_fr_XYZ[:, 0]
    processedDataFrame.loc[:, ('handQuatVelocity', 'Y')
                           ] = handQuatVelocity_fr_XYZ[:, 1]
    processedDataFrame.loc[:, ('handQuatVelocity', 'Z')
                           ] = handQuatVelocity_fr_XYZ[:, 2]
    processedDataFrame.loc[:, ('handQuatVelocity', 'W')
                           ] = handQuatVelocity_fr_XYZ[:, 3]

    return processedDataFrame


def calculateBallExpansionRate(rawDataFrame, processedDataFrame, ballDiameter=0.010):

    processedDataFrame['ballSize'] = (
        180 / np.pi) * np.arctan(ballDiameter * 0.5 / processedDataFrame['ballDepth'])
    ballSize = processedDataFrame['ballSize']
    ballSize_shifted = np.roll(ballSize, -1, axis=0)

    timeArray = rawDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted - timeArray

    processedDataFrame['ballExpansionRate'] = np.divide(
        ballSize_shifted - ballSize, timeDiff)

    return processedDataFrame


def calculateBallDepth(rawDataFrame, processedDataFrame):

    processedDataFrame['eyeBallDistance',
                       'X'] = rawDataFrame['ballPos', 'X'] - rawDataFrame['viewPos', 'X']
    processedDataFrame['eyeBallDistance',
                       'Y'] = rawDataFrame['ballPos', 'Y'] - rawDataFrame['viewPos', 'Y']
    processedDataFrame['eyeBallDistance',
                       'Z'] = rawDataFrame['ballPos', 'Z'] - rawDataFrame['viewPos', 'Z']

    processedDataFrame['ballDepth'] = processedDataFrame['eyeBallDistance'].apply(
        calculateNorm, axis=1)

    ballDepth = processedDataFrame['ballDepth']
    ballDepth_shifted = np.roll(ballDepth, -1, axis=0)
    ballDepth = ballDepth_shifted - ballDepth

    timeArray = rawDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted - timeArray

    processedDataFrame['ballDepthVelocity'] = np.divide(ballDepth, timeDiff)

    return processedDataFrame


def calculateBallVelocity(rawDataFrame, processedDataFrame):

    eyeToScreenDistance = 0.0725

    ballPosition_fr_XYZ = np.array([(180 / np.pi) * np.arctan(processedDataFrame['ballOnScreen']['X'].values.astype(float) / eyeToScreenDistance),
                                    (180 / np.pi) * np.arctan(processedDataFrame['ballOnScreen']['Y'].values.astype(
                                        float) / eyeToScreenDistance),
                                    (180 / np.pi) * np.arctan(processedDataFrame['ballOnScreen']['Z'].values.astype(float) / eyeToScreenDistance)], dtype=float).T
    ballPosition_fr_XYZ_shifted = np.roll(ballPosition_fr_XYZ, -1, axis=0)

    timeArray = rawDataFrame.frameTime.values
    timeArray_shifted = np.roll(timeArray, -1)
    timeDiff = timeArray_shifted - timeArray
    # print timeArray_shifted[-1], '\n'
    # print timeArray[-1], '\n'
    # print timeDiff
    # print global_metricCycPOR_fr_XYZ[0:5,:], '\n'
    # print metricCycPOR_fr_XYZ_shifted[0:5,:]
    #plotMyData_Scatter3D(metricCycPOR_fr_XYZ_shifted, label = 'Cyc Gaze Points', color = 'r', marker = 'o', axisLabels=['X [m]', 'Y [m]', 'Z [m]'])
    #print('Ball Pos', ballPosition_fr_XYZ[0:4,:])
    #print('\nBall Pos Shifted', ballPosition_fr_XYZ_shifted[0:4,:])
    #print('Before Division',ballPosition_fr_XYZ.shape)
    ballAngle_fr_XYZ = ballPosition_fr_XYZ_shifted - ballPosition_fr_XYZ
    #print('After Division',ballAngle_fr_XYZ.shape)
    # for v1,v2 in zip(ballPosition_fr_XYZ, ballPosition_fr_XYZ_shifted):
    #    ballAngle_fr.append(vectorAngle(v1, v2))
    ballVelocity_fr_XYZ = np.zeros((ballAngle_fr_XYZ.shape))
    for i in range(3):
        ballVelocity_fr_XYZ[:, i] = np.divide(ballAngle_fr_XYZ[:, i], timeDiff)

    #print('\nFinal Shape: ',ballVelocity_fr_XYZ.shape)
    #print('\nFinal diff: ',ballAngle_fr_XYZ.shape)

    processedDataFrame.loc[:, ('ballAngularVelocity', 'X')
                           ] = ballVelocity_fr_XYZ[:, 0]
    processedDataFrame.loc[:, ('ballAngularVelocity', 'Y')
                           ] = ballVelocity_fr_XYZ[:, 1]
    processedDataFrame.loc[:, ('ballAngularVelocity', 'Z')
                           ] = ballVelocity_fr_XYZ[:, 2]
    return processedDataFrame


def extractFeatureFromPickle(pickleFileName):

    print('Reading the All Subject Pickle File ... \n ', pickleFileName)
    eyeToScreenDistance = 0.0725
    ballDiameter = 0.01
    df = pd.read_pickle(pickleFileName)
    print('Raw Dataset Read in!')
    rawDataFrame = df['raw']
    processedDataFrame = df['processed']
    calibDataFrame = df['calibration']
    trialInfoDataFrame = df['trialInfo']

    processedDataFrame = calculateBallVelocity(
        rawDataFrame, processedDataFrame)
    processedDataFrame = calculateBallDepth(rawDataFrame, processedDataFrame)
    processedDataFrame = calculateBallExpansionRate(
        rawDataFrame, processedDataFrame, ballDiameter=0.010)
    processedDataFrame = calculateHandVelocity(
        rawDataFrame, processedDataFrame)
    processedDataFrame = calculateGazeVelocity(
        rawDataFrame, processedDataFrame)
    processedDataFrame = calculateHeadVelocity(
        rawDataFrame, processedDataFrame)

    trialStartIdx = processedDataFrame[processedDataFrame['eventFlag']
                                       == 'trialStart'].index.tolist()
    ballOffIdx = processedDataFrame[processedDataFrame['eventFlag']
                                    == 'ballRenderOff'].index.tolist()
    ballOnIdx = processedDataFrame[processedDataFrame['eventFlag']
                                   == 'ballRenderOn'].index.tolist()
    ballOnPaddleIdx = processedDataFrame[processedDataFrame['eventFlag']
                                         == 'ballOnPaddle'].index.tolist()

    ballCrossingIdx = np.zeros(len(trialInfoDataFrame), dtype=int)
    ballCrossingIdx[trialInfoDataFrame.ballCaughtQ.values ==
                    True] = processedDataFrame[processedDataFrame['eventFlag'] == 'ballOnPaddle'].index.tolist()
    ballCrossingIdx[trialInfoDataFrame.ballCaughtQ.values ==
                    False] = processedDataFrame[processedDataFrame['eventFlag'] == 'ballCrossingPaddle'].index.tolist()
    trialInfoDataFrame.loc[:, ('ballCrossingIndex', '')] = ballCrossingIdx

    print('trialStart = ', trialStartIdx)
    print('crossing = ', ballCrossingIdx[:])
    #print (ballCrossingIdx[135], ballCrossingIdx[136], ballCrossingIdx[137])
    # return

    successDataSet = dict()
    failDataSet = dict()
    allDataSet = dict()
    newSuccessDataSet = dict()
    newFailDataSet = dict()

    rawDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                    False, ('ballPos', 'X')] = 0.0
    rawDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                    False, ('ballPos', 'Y')] = 0.0
    rawDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                    False, ('ballPos', 'Z')] = 0.0
    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballDepth', '')] = 0.0
    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballSize', '')] = 0.0

    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballVelocity')] = 0.0
    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballAngularVelocity', 'X')] = 0.0
    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballAngularVelocity', 'Y')] = 0.0
    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballAngularVelocity', 'Z')] = 0.0
    processedDataFrame.ix[rawDataFrame.isBallVisibleQ.values ==
                          False, ('ballExpansionRate', '')] = 0.0

    allCounter = 0
    sCounter = 0
    fCounter = 0
    allDataSet = dict()
    successDataSet = dict()
    failDataSet = dict()
    for i in range(len(trialInfoDataFrame)):
        allDataSet['trial' + str(
            allCounter)] = rawDataFrame.trialType.values[trialInfoDataFrame.firstFrame.values[i] + 5]
        sCounter = sCounter + 1
        if (trialInfoDataFrame.ballCaughtQ.values[i]) is True:
            successDataSet['trial' + str(
                sCounter)] = rawDataFrame.trialType.values[trialInfoDataFrame.firstFrame.values[i] + 5]
            sCounter = sCounter + 1
        else:
            failDataSet['trial' + str(
                fCounter)] = rawDataFrame.trialType.values[trialInfoDataFrame.firstFrame.values[i] + 5]
            fCounter = fCounter + 1
    pd.to_pickle(allDataSet, r'.\DataSets\trialType_All.pickle')
    pd.to_pickle(successDataSet, r'.\DataSets\trialType_Success.pickle')
    pd.to_pickle(failDataSet, r'.\DataSets\trialType_Fail.pickle')
    print('trial Type Saved into Pickle')

    allCounter = 0
    sCounter = 0
    fCounter = 0
    for i in range(len(trialInfoDataFrame)):
        print('Trial Number: ', i)
        # trialInfoDataFrame.ballCrossingIndex.values[i]
        endFrame = ballCrossingIdx[i]

        x = np.array(rawDataFrame.ballPos.X.values[trialStartIdx[i]:endFrame])
        y = np.array(rawDataFrame.ballPos.Y.values[trialStartIdx[i]:endFrame])
        print('Length of Vector:\n', len(x), len(y))
        print('Start Idx', trialStartIdx[i])
        print('End Frame', endFrame)

        if len(processedDataFrame.loc[trialStartIdx[i]:endFrame, ('ballOnScreen', 'X')]) != len((180 / np.pi) * np.arctan(x.astype(float) / eyeToScreenDistance)):
            #print('Not Equal!')
            #print('Len Data1 = ', len((180/np.pi)*np.arctan(x.astype(float)/eyeToScreenDistance)))
            #print('Len Data2 = ',len(processedDataFrame.loc[trialStartIdx[i]:endFrame, ('ballOnScreen','X')]))
            processedDataFrame.loc[trialStartIdx[i]:endFrame - 1, ('ballOnScreen', 'X')] = (
                180 / np.pi) * np.arctan(x.astype(float) / eyeToScreenDistance)
            processedDataFrame.loc[trialStartIdx[i]:endFrame - 1, ('ballOnScreen', 'Y')] = (
                180 / np.pi) * np.arctan(y.astype(float) / eyeToScreenDistance)
        else:
            print('Equal!')
            #print('Len Data1 = ', len((180/np.pi)*np.arctan(x.astype(float)/eyeToScreenDistance)))
            #print('Len Data2 = ',len(processedDataFrame.loc[trialStartIdx[i]:endFrame, ('ballOnScreen','X')]))
            processedDataFrame.loc[trialStartIdx[i]:endFrame, ('ballOnScreen', 'X')] = (
                180 / np.pi) * np.arctan(x.astype(float) / eyeToScreenDistance)
            processedDataFrame.loc[trialStartIdx[i]:endFrame, ('ballOnScreen', 'Y')] = (
                180 / np.pi) * np.arctan(y.astype(float) / eyeToScreenDistance)

        handPosition = rawDataFrame.paddlePos.values[trialStartIdx[i]:endFrame]
        handQuaternion = rawDataFrame.paddleQuat.values[trialStartIdx[i]:endFrame]
        gazePosition = processedDataFrame.gazePoint.values[trialStartIdx[i]:endFrame, 0:2]
        headPosition = rawDataFrame.viewPos.values[trialStartIdx[i]:endFrame]
        headQuaternion = rawDataFrame.viewQuat.values[trialStartIdx[i]:endFrame]

        handVelocity = processedDataFrame.handVelocity.values[trialStartIdx[i]:endFrame]
        handQuatVelocity = processedDataFrame.handQuatVelocity.values[trialStartIdx[i]:endFrame]
        gazeVelocity = processedDataFrame.gazeVelocity.values[trialStartIdx[i]:endFrame]
        ballPosition = processedDataFrame.ballOnScreen.values[trialStartIdx[i]:endFrame, 0:2]
        ballDepth = processedDataFrame.ballDepth.values[trialStartIdx[i]:endFrame]
        ballVelocity = processedDataFrame.ballAngularVelocity.values[
            trialStartIdx[i]:endFrame, 0:2]
        ballDepthVelocity = processedDataFrame.ballDepthVelocity.values[trialStartIdx[i]:endFrame]
        ballSize = processedDataFrame.ballSize.values[trialStartIdx[i]:endFrame]
        ballExpansionRate = processedDataFrame.ballExpansionRate.values[trialStartIdx[i]:endFrame]
        headVelocity = np.array([processedDataFrame.headVelocity.X.values[trialStartIdx[i]:endFrame],
                                 processedDataFrame.headVelocity.Y.values[trialStartIdx[i]:endFrame],
                                 processedDataFrame.headVelocity.Z.values[trialStartIdx[i]:endFrame]]).T
        headQuatVelocity = processedDataFrame.headQuatVelocity.values[trialStartIdx[i]:endFrame]
        #x = processedDataFrame.gazePoint.X.values[trialStartIdx[i]:endFrame]
        #y = processedDataFrame.gazePoint.Y.values[trialStartIdx[i]:endFrame]
        #processedDataFrame.loc[trialStartIdx[i]:endFrame-1, ('gazePoint','X')] = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
        #processedDataFrame.loc[trialStartIdx[i]:endFrame-1, ('gazePoint','Y')] = (180/np.pi)*np.arctan(y/eyeToScreenDistance)

        #ballVelocity = processedDataFrame.ballVelocity.values[trialStartIdx[i]:endFrame]
        #gazeVelocity = processedDataFrame.cycGazeVelocity.values[trialStartIdx[i]:endFrame]
        renderingFlag = rawDataFrame.isBallVisibleQ.values[trialStartIdx[i]:endFrame]

        dataBase = np.zeros((ballPosition.shape[0], 41))
        dataBase[:, 0:3] = handPosition
        dataBase[:, 3:7] = handQuaternion
        dataBase[:, 7:9] = gazePosition[:, 0:2]
        dataBase[:, 9:12] = headPosition
        dataBase[:, 12:16] = headQuaternion

        dataBase[:, 16:19] = handVelocity
        dataBase[:, 19:23] = handQuatVelocity
        dataBase[:, 23:25] = gazeVelocity
        dataBase[:, 25:27] = ballPosition
        dataBase[:, 27] = ballDepth
        dataBase[:, 28:30] = ballVelocity
        dataBase[:, 30] = ballDepthVelocity
        dataBase[:, 31] = ballSize
        dataBase[:, 32] = ballExpansionRate
        dataBase[:, 33:36] = headVelocity
        dataBase[:, 36:40] = headQuatVelocity
        dataBase[:, 40] = renderingFlag

        preBD = trialInfoDataFrame.preBlankDur.values[i]
        postBD = trialInfoDataFrame.postBlankDur.values[i]
        trialType = findTrialType(preBD, postBD)
        print('Trial[', trialType, '] = ',
              ballCrossingIdx[i] - trialStartIdx[i])
        allDataSet['type_' + trialType +
                   '_' + str(allCounter)] = dataBase
        #successDataSet['trialType' + str(sCounter)] = str(trialInfoDataFrame.preBlankDur.values[i]) + '_' + str(trialInfoDataFrame.postBlankDur.values[i])
        allCounter = allCounter + 1

        if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
            successDataSet['trial' + str(sCounter)] = dataBase
            newSuccessDataSet['type_' + trialType +
                              '_' + str(sCounter)] = dataBase
            #successDataSet['trialType' + str(sCounter)] = str(trialInfoDataFrame.preBlankDur.values[i]) + '_' + str(trialInfoDataFrame.postBlankDur.values[i])
            sCounter = sCounter + 1
        else:
            failDataSet['trial' + str(fCounter)] = dataBase
            newFailDataSet['type_' + trialType +
                           '_' + str(fCounter)] = dataBase
            #successDataSet['trialType' + str(sCounter)] = str(trialInfoDataFrame.preBlankDur.values[i]) + '_' + str(trialInfoDataFrame.postBlankDur.values[i])
            fCounter = fCounter + 1

    print('Saving the "OLD" data not done! Hacked!')
    #pd.to_pickle(successDataSet,  r'.\Datasets\Success\DataSet_Formatted_Success.pickle')
    #pd.to_pickle(failDataSet,  r'.\Datasets\Fail\DataSet_Formatted_Fail.pickle')
    pd.to_pickle(allDataSet,
                 r'.\DataSets\DataSet_All.pickle')
    pd.to_pickle(newSuccessDataSet,
                 r'.\DataSets\DataSet_Success.pickle')
    pd.to_pickle(newFailDataSet,  r'.\DataSets\DataSet_Fail.pickle')
    print('Formatted Data Sets Per Trial Saved into Pickle', )
    print('Fail Count = ', fCounter, '\nSuccess Count = ', sCounter)

    return


def resizeDataSet(dataSetFileName, inputSeqLength):

    if (inputSeqLength == 0):
        print('Skip the Resize Data Function!')
        return

    print('Formatting the Data Set for Window Size of ' +
          str(inputSeqLength) + ' Samples:\n')

    rawDataSet = pd.read_pickle(dataSetFileName)
    print(len(rawDataSet))

    allTrialList = sorted(list(rawDataSet.keys()))
    trainTrialList = allTrialList[1:91]
    print(trainTrialList)
    print('Len = ', len(allTrialList))
    outputTrainDataSet = np.zeros((100000, inputSeqLength, 41))
    outputTrainTrialType = np.chararray(100000, itemsize=9)  # np.array([])
    trialNumberList = np.chararray(100000, itemsize=9)  # np.array([])
    cnt = 0
    for trialNumber in trainTrialList:
        print('Trial Number =', trialNumber)
        currentTrial = rawDataSet[trialNumber]
        for i in range(currentTrial.shape[0] - inputSeqLength + 1):
            tempVar = currentTrial[i:i + inputSeqLength, :]
            outputTrainDataSet[cnt, :, :] = tempVar
            outputTrainTrialType[cnt] = trialNumber[0:8]
            trialNumberList[cnt] = trialNumber[8:]
            cnt = cnt + 1

    outputTrainDataSet = outputTrainDataSet[0:cnt, :, :]
    outputTrainTrialType = outputTrainTrialType[0:cnt]
    trialNumberList = trialNumberList[0:cnt]
    print('cnt = ', cnt)
    #print (rawDataSet[trainTrialList[0]].shape)
    print('Train:\n')
    print(outputTrainDataSet.shape)
    print(outputTrainTrialType.shape)
    print(trialNumberList.shape)
    print(outputTrainTrialType[0:5])
    print(trainTrialList[0:5])
    print(trialNumberList[0:5])

    pd.to_pickle(outputTrainDataSet, r'.\Datasets\Train_DataSet_' +
                 str(inputSeqLength) + r'.pickle')
    pd.to_pickle(outputTrainTrialType, r'.\Datasets\Train_TrialType_' +
                 str(inputSeqLength) + r'.pickle')
    pd.to_pickle(trialNumberList, r'.\Datasets\Train_TrialNumber_' +
                 str(inputSeqLength) + r'.pickle')

    testTrialList = allTrialList[91:136]
    print(testTrialList)
    outputTestDataSet = np.zeros((100000, inputSeqLength, 41))
    outputTestTrialType = np.chararray(100000, itemsize=9)  # np.array([])
    trialNumberList = np.chararray(100000, itemsize=9)  # np.array([])
    cnt = 0
    for trialNumber in testTrialList:
        print('Trial Number =', trialNumber)
        currentTrial = rawDataSet[trialNumber]
        # sys.stdout.flush()
        for i in range(currentTrial.shape[0] - inputSeqLength + 1):
            tempVar = currentTrial[i:i + inputSeqLength, :]
            outputTestDataSet[cnt, :, :] = tempVar
            outputTestTrialType[cnt] = trialNumber[0:8]
            trialNumberList[cnt] = trialNumber[8:]
            cnt = cnt + 1

    outputTestDataSet = outputTestDataSet[0:cnt, :, :]
    outputTestTrialType = outputTestTrialType[0:cnt]
    trialNumberList = trialNumberList[0:cnt]
    print('cnt = ', cnt)
    #print (rawDataSet[trainTrialList[0]].shape)
    print('Test:\n')
    print(outputTestDataSet.shape)
    print(outputTestTrialType.shape)
    print(trialNumberList.shape)
    print(outputTestTrialType[0:5])
    print(trialNumberList[0:5])
    print(allTrialList[91:96])

    pd.to_pickle(outputTestDataSet, r'.\Datasets\Test_DataSet_' +
                 str(inputSeqLength) + r'.pickle')
    pd.to_pickle(outputTestTrialType, r'.\Datasets\Test_TrialType_' +
                 str(inputSeqLength) + r'.pickle')
    pd.to_pickle(trialNumberList, r'.\Datasets\Test_TrialNumber_' +
                 str(inputSeqLength) + r'.pickle')


def restructureTestDataSet(rawDataSetFileName, trialTypeFileName, trialNumberFileName):

    trialNumber = pd.read_pickle(trialTypeFileName)
    trialNumber = pd.read_pickle(trialNumberFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    #print (trialType[0:600])
    # = np.unique(trialType)
    _, idx = np.unique(trialNumber, return_index=True)
    uniqueTrialType = trialNumber[np.sort(idx)]
    print('Unique Keys=\n', sorted(uniqueTrialType))
    outputTestDataSet = dict()
    for myKey in uniqueTrialType:
        outputTestDataSet[myKey] = rawDataSet[myKey]

    #print (outputTestDataSet.keys()[0:5])
    pd.to_pickle(outputTestDataSet,
                 r'.\Datasets\Success\Test_DataSet_Success_ByTrial.pickle')
    #print (outputTestDataSet[myKey])
    print(outputTestDataSet[myKey].shape)

    return outputTestDataSet


def SaveValuesIntoNpArray(x, fname):
    """Save matrix of image into numpy array. x - image matrix, fname - path/name of future file without extension"""
    fname = fname + '.npy'
    np.save(fname, x, allow_pickle=True, fix_imports=True)


def calculateStats(dataSet):

    trialList = list(dataSet.keys())
    allTrials = np.zeros((1, 10))
    count = 0
    for trialNumber in trialList:
        tempVar = dataSet[trialNumber]
        allTrials = np.vstack((allTrials, tempVar))
        count = count + dataSet[trialNumber].shape[0]
    allTrials = np.delete(allTrials, 0, 0)
    print(count)
    print(allTrials.shape)
    mean = np.mean(allTrials, axis=0)
    std = np.std(allTrials, axis=0)
    print('Mean=\n', mean)
    print('STD=\n', std)
    return [mean, std]


def normalizeDataSet(dataSet, mean, std):
    trialList = list(dataSet.keys())

    count = 0
    normalizedDataSet = dict()
    for trialNumber in trialList:
        normalizedDataSet[trialNumber] = dataSet[trialNumber] - \
            np.repeat(mean, dataSet[trialNumber].shape[0], axis=0)
        normalizedDataSet[trialNumber] = np.divide(
            normalizedDataSet[trialNumber], np.repeat(std, dataSet[trialNumber].shape[0], axis=0))

    print(dataSet[trialNumber][0:3, :])
    print(normalizedDataSet[trialNumber][0:3, :])
    return normalizedDataSet


rawDataSetFileName = r'.\Datasets\Success\DataSet_Formatted_Success.pickle'
dataSetFileName = rawDataSetFileName
trialTypeFileName = r'.\Datasets\Success\trialType_Success.pickle'


# trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_6.pickle'
# testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_6.pickle'
# trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_6.pickle'
# trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_6.pickle'
#
# trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_21.pickle'
# testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_21.pickle'
# trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_21.pickle'
# trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_21.pickle'

print('Running Main FormatDataSet.py ... \n')
print('======> Purpose is to Generate Per Subject Models ... \n')

#pickleFileName = r'.\Datasets\AllSubjects_2.pickle'
#pickleFileName = r'.\Datasets\exp_data-2016-4-19-14-4.pickle'
subjectID = r'2016-5-3-12-52'
subjectID = r'2016-5-5-13-7'
#pickleFileName = r'.\Datasets\2016-5-3-12-52\exp_data-2016-5-3-12-52.pickle'
pickleFileName = r'.\Datasets\2016-5-5-13-7\exp_data-2016-5-5-13-7.pickle'


#windowSize = [15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80]
#windowSize = [45, 50, 55, 60, 65, 70, 75, 80]
windowSize = [80]
print('Reading the Subject Pickle File ... \n ', pickleFileName)
eyeToScreenDistance = 0.0725
ballDiameter = 0.01


# df = pd.read_pickle(pickleFileName)
# print('Raw Dataset Read in!')
# rawDataFrame = df['raw']
# processedDataFrame = df['processed']
# calibDataFrame = df['calibration']
# trialInfoDataFrame = df['trialInfo']
#
# myTrialList = ['t1','t2','t3','t4','t5','t6','t7','t8','t9']
# for i in myTrialList:
#     print(i,' = ')
#     print(np.unique(rawDataFrame.preBlankDur.values[rawDataFrame.trialType.values == i]))
#     print(np.unique(rawDataFrame.postBlankDur.values[rawDataFrame.trialType.values == i]))
#     print('\n\n')
# extractFeatureFromPickle(pickleFileName)
dataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
print(dataSetFileName)
trialTypeFileName = r'.\Datasets\2016-5-5-13-7\trialType_All.pickle'
print(trialTypeFileName)
for inputSeqLength in windowSize:
    resizeDataSet(dataSetFileName, inputSeqLength)

'''
inputSequenceList = [3, 6, 9, 12, 15, 18, 21]
inputSeqLength = 0
#myList = list(trialType.keys())
#print (sorted(myList))

'''
