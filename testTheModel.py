'''
Created on Mar 24, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
import math
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD, RMSprop, Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN


def calculateMSE(testData, modelData):

    print(testData.shape)
    print(modelData.shape)
    #mse = (np.sum(np.power(testData - modelData,2), axis = 0))/testData.shape[0]
    #mse = (np.sum(np.abs(testData - modelData), axis = 0))/testData.shape[0]
    mse = (np.mean(np.abs(testData - modelData), axis=0))
    print('mse Shape', mse.shape)
    print('MSE = \n', mse)
    std = np.std(testData - modelData, axis=0)
    print('std Shape', std.shape)
    print('STD = \n', std)

    return mse, std


def plotMSE(testOutputList, modelOutputList):

    # print('Test',testOutputList)
    # print('Model',modelOutputList)

    meanMatix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    stdMatrix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    for i in range(len(testOutputList)):
        #print('Shapes', testOutputList[i].shape, modelOutputList[i].shape)
        mean, std = calculateMSE(testOutputList[i], modelOutputList[i])
        meanMatix[i, :] = mean
        stdMatrix[i, :] = std

    #x = np.arange(len(testOutputList))
    #x = x/75.
    x = np.array([15, 20, 25, 30, 35, 40, 45]) / 75.0
    meanHandX = meanMatix[:, 0]
    meanHandY = meanMatix[:, 1]
    meanHandZ = meanMatix[:, 2]

    stdHandX = stdMatrix[:, 0]
    stdHandY = stdMatrix[:, 1]
    stdHandZ = stdMatrix[:, 2]

    meanGazeX = meanMatix[:, 7]
    meanGazeY = meanMatix[:, 8]

    stdGazeX = stdMatrix[:, 7]
    stdGazeY = stdMatrix[:, 8]

    meanHeadX = meanMatix[:, 9]
    meanHeadY = meanMatix[:, 10]
    meanHeadZ = meanMatix[:, 11]

    stdHeadX = stdMatrix[:, 9]
    stdHeadY = stdMatrix[:, 10]
    stdHeadZ = stdMatrix[:, 11]

    SMALL_SIZE = 2
    MEDIUM_SIZE = 18
    BIGGER_SIZE = 18
    ERROR_BAR_LINE_SIZE = 3
    FIG_X_SIZE = 11
    FIG_Y_SIZE = 6

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.errorbar(x, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b',
                 label='Hand X', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.errorbar(x + 0.2 / 75, meanHandY, yerr=stdHandY, fmt='--ro',
                 ecolor='r', label='Hand Y', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.errorbar(x + 0.4 / 75, meanHandZ, yerr=stdHandZ, fmt='--go',
                 ecolor='g', label='Hand Z', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Integration Time")
    plt.xlabel('Integration Time [s]')
    plt.ylabel('Model Hand Position Error [m]')

    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandErrorVsIntegrationTime.png')
    plt.savefig('HandErrorVsIntegrationTime.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.errorbar(x, meanGazeX, yerr=stdGazeX, fmt='--bo', ecolor='b',
                 label='Gaze Azimuth', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.errorbar(x + 0.2 / 75, meanGazeY, yerr=stdGazeY, fmt='--ro',
                 ecolor='r', label='Gaze Elevation', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Gaze Prediction Error Vs. Integration Time")
    plt.xlabel('Integration Time [s]')
    plt.ylabel('Model Gaze Position Error [degree]')

    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('GazeErrorVsIntegrationTime.png')
    plt.savefig('GazeErrorVsIntegrationTime.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.errorbar(x, meanHeadX, yerr=stdHeadX, fmt='--bo', ecolor='b',
                 label='Head X', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.errorbar(x + 0.2 / 75, meanHeadY, yerr=stdHeadY, fmt='--ro',
                 ecolor='r', label='Head Y', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.errorbar(x + 0.4 / 75, meanHeadZ, yerr=stdHeadZ, fmt='--go',
                 ecolor='g', label='Head Z', elinewidth=ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Head Prediction Error Vs. Integration Time")
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

    plt.xlabel('Integration Time [s]')
    plt.ylabel('Model Head Position Error [m]')
    plt.savefig('HeadErrorVsIntegrationTime.png')
    plt.savefig('HeadErrorVsIntegrationTime.svg', format='svg', dpi=1000)


def PlotModelPrediction(testing_Output, modelOutput, figureName):
    """function for loss plot
    fname_Loss - full path to training loss
    fname_valLoss - full path to training loss 
    """
    handMarkerModel = '^b'
    handMarkerHuman = 'og'
    gazeMarkerModel = '^b'
    gazeMarkerHuman = 'or'
    frameRange = range(500, 1500)
    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange, 0])), testing_Output[frameRange,
                                                                           0], handMarkerHuman, linewidth=4, label='Subject Hand X')
    plt.plot(np.arange(len(modelOutput[frameRange, 0])), modelOutput[frameRange,
                                                                     0], handMarkerModel, linewidth=4, label='Model Hand X')
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Hand X [m]')
    plt.savefig(str(figureName) + '_Hand_X.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange, 1])), testing_Output[frameRange,
                                                                           1], handMarkerHuman, linewidth=4, label='Subject Hand Y')
    plt.plot(np.arange(len(modelOutput[frameRange, 1])), modelOutput[frameRange,
                                                                     1], handMarkerModel, linewidth=4, label='Model Hand Y')
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Hand Y [m]')
    plt.savefig(str(figureName) + '_Hand_Y.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange, 2])), testing_Output[frameRange,
                                                                           2], handMarkerHuman, linewidth=4, label='Subject Hand Z')
    plt.plot(np.arange(len(modelOutput[frameRange, 2])), modelOutput[frameRange,
                                                                     2], handMarkerModel, linewidth=4, label='Model Hand Z')
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Hand Z [m]')
    plt.savefig(str(figureName) + '_Hand_Z.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange, 7])), testing_Output[frameRange,
                                                                           7], gazeMarkerHuman, linewidth=4, label='Subject Gaze Azimuth')
    plt.plot(np.arange(len(modelOutput[frameRange, 7])), modelOutput[frameRange,
                                                                     7], gazeMarkerModel, linewidth=4, label='Model Gaze Azimuth')
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Gaze Azimuth [degree]')
    plt.savefig(str(figureName) + '_Gaze_X.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange, 8])), testing_Output[frameRange,
                                                                           8], gazeMarkerHuman, linewidth=4, label='Subject Gaze Elevation')
    plt.plot(np.arange(len(modelOutput[frameRange, 8])), modelOutput[frameRange,
                                                                     8], gazeMarkerModel, linewidth=4, label='Model Gaze Elevation')
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Gaze Elevation [degree]')
    plt.savefig(str(figureName) + '_Gaze_Y.png')

    # plt.show()


def testModel(model=None, rawDataSetFileName=None, testDataSetFileName=None, data=None, epochNumber=None):

    if testDataSetFileName is None:
        print('No Path Specified for Training Set!!\n ==> Quit Training\n\n')
        return None
    else:
        print('Reading Testing Data Set: ', testDataSetFileName)
        testDataSet = pd.read_pickle(testDataSetFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    #print('feature 1 ', testDataSet[0,:,5])
    [mean, std] = calculateStats(rawDataSet)
    #print('\nmean = ',mean)
    #print('\nstd = ',std)
    feat_mean = mean
    feat_std = std
    #feat_mean = np.array([ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737])
    #feat_std =  np.array([ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025])

    for index in range(len(feat_mean)):
        testDataSet[:, :, index] -= feat_mean[index]
        testDataSet[:, :, index] = testDataSet[:, :, index] / (feat_std[index])
    #print('feature 1 ', testDataSet[0,:,5],'\n')

    #testDataSet = normalizeDataSet(testDataSet, feat_mean,  feat_std)
    #testDataSet = testDataSet - np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0)
    #testDataSet = np.divide(testDataSet, np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0))
    dataSetDimension = testDataSet.shape
    print(dataSetDimension)

    #print (testDataSet['t1'][0].keys())
    #print (testDataSet['t1'][0].items())

    #training_Input = testDataSet[:,0:dataSetDimension[1],:]
    #training_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
    testing_Input = testDataSet[:, 0:dataSetDimension[1], :]
    testing_Output = testDataSet[:, dataSetDimension[1] - 1, 0:16]

    #X_train, y_train, X_test, y_test = data
    #cb = EarlyStopping(monitor='val_loss', patience=7, verbose=0, mode='auto')
    #history = model.fit(training_Input, training_Output,batch_size=128, nb_epoch=epochNumber, callbacks = [cb], validation_split=0.15, shuffle=True)

    #print('Running Model Evaluation ...')
    #score, accuracy = model.evaluate(testing_Input, testing_Output, batch_size = 128, verbose = 0 )
    # print(accuracy)
    # print(score)
    print('Running Model Prediction ...')
    modelOutput = model.predict(testing_Input, verbose=0)
    print(modelOutput.shape)
    print(testing_Output.shape)
    #mse = np.mse(testing_Output.T, modelOutput.T)
    mse = np.array(
        np.mean(np.power(testing_Output - modelOutput, 2), axis=0), dtype=float)

    print(mse)
    print(min(mse))
    print(max(mse))
    print(np.std(mse))
    figureName = r'.\Results\Figures\PerSubjectModel\modelOutput'
    # Un-normalizing the Data
    for index in range(modelOutput.shape[1]):
        modelOutput[:, index] = modelOutput[:, index] * (feat_std[index])
        modelOutput[:, index] += feat_mean[index]
        testing_Output[:, index] = testing_Output[:, index] * (feat_std[index])
        testing_Output[:, index] += feat_mean[index]

    PlotModelPrediction(testing_Output, modelOutput, figureName)

    '''
    predicted = model.predict(X_test)
    predicted = np.array(predicted)
   
    mse_predict = mean_squared_error(predicted, y_test, multioutput='raw_values')
    
    for item in history.history['loss']:
        f.write('{}\n'.format(item))

    for item in history.history['val_loss']:
        f.write('{}\n'.format(item))
    
    
    '''

    return model, modelOutput, testing_Output


if __name__ == "__main__":

    trainOnWhat = 'All'  # , 'Success', 'Fail'
    subjectID = '2016-5-3-12-52'
    subjectID = '2016-5-5-13-7'
    rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
    rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
    trainDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'

    #inputSequenceList = [3, 6, 9, 12, 15, 18, 21]
    # [15, 20, 25, 30, 35, 40, 45]#, 50, 55, 60, 65, 70, 75]#, 80]
    windowSize = [80]
    #learningRateList = [0.0001, 0.0005]
    learningRateList = [0.0001]
    epochNumberList = [500]
    modelOutputList = []
    testOutputList = []

    for epochNumber in epochNumberList:
        for learningRate in learningRateList:
            for inputSeqLength in windowSize:
                print('\n\nTresting on Input Sequence Length = ', inputSeqLength)
                print('Learning Rate = ', learningRate)
                print('Epoch Number = ', epochNumber)
                if (trainOnWhat == 'Success'):
                    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_' + \
                        str(inputSeqLength) + '.pickle'
                elif(trainOnWhat == 'Fail'):
                    trainDataSetFileName = r'.\Datasets\Fail\Train_DataSet_Fail_' + \
                        str(inputSeqLength) + '.pickle'
                elif(trainOnWhat == 'All'):
                    rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
                    trainDataSetFileName = r'.\Datasets\2016-5-5-13-7\Train_DataSet_80.pickle'
                    testDataSetFileName = r'.\Datasets\2016-5-5-13-7\Test_DataSet_80.pickle'
                    weights_path = r'.\Results\Weights\PerSubjectModel\2016-5-5-13-7\weights_' + \
                        str(inputSeqLength - 35) + '_1_0.0001_500'
                print(weights_path)
                model = createRNN(inputSeqLength, weights_path, learningRate)
                print('RNN-LSTM Model Created ...\n')
                model, modelOutput, testingOutput = testModel(
                    model, rawDataSetFileName, testDataSetFileName, epochNumber=epochNumber)
                pd.to_pickle(modelOutput,  r'.\Results\ModelOutput\PerSubjectModel\modelOutput_' +
                             str(inputSeqLength) + '_0.0001_500.pickle')
                modelOutputList.append(modelOutput)
                testOutputList.append(testingOutput)
                print('Testing Shape', testingOutput.shape)
                print('Model Shape', modelOutput.shape)
                #plotMSE(modelOutputList, modelOutputList, timeStep)

    #plotMSE(testOutputList, modelOutputList)
