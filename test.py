'''
Created on Feb 4, 2017

@author: Kamran Binaee
'''
import pandas as pd
import numpy as np
from pyquaternion import Quaternion
import matplotlib.pyplot as plt
pickleFileName = r'.\Datasets\2016-4-19-14-4\DataSet_All-2016-4-19-14-4.pickle'


print('Reading the Subject Pickle File ... \n ', pickleFileName)

df = pd.read_pickle(pickleFileName)

allKeys = list(df.keys())
# print(allKeys)
for trialKey in allKeys:
    print(df[trialKey].shape)

idx = 35

dataSet = df[allKeys[idx]]
