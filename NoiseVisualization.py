'''
Created on Apr 21, 2017

@author: Kamran Binaee
'''
'''
Created on Apr 11, 2017

@author: Kamran Binaee
'''
'''
Created on Apr 9, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN
from keras.utils.np_utils import accuracy

import plotly
from plotly.graph_objs import Scatter, Layout
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from IPython.lib.tests.test_pretty import MyList

print (plotly.__version__)

plotly.offline.init_notebook_mode()

def calculateMSE(testData, modelData):
    
    print(testData.shape)
    print(modelData.shape)
    #mse = (np.sum(np.power(testData - modelData,2), axis = 0))/testData.shape[0]
    mse = (np.sum(np.abs(testData - modelData), axis = 0))/testData.shape[0]
    print('mse Shape', mse.shape)
    #print(mse)
    std = np.std(testData - modelData, axis = 0)
    print('std Shape', std.shape)
    #print(std)
    
    return mse, std
    
def plotMSE(testOutputList, modelOutputList, figName):
    
    #print('Test',testOutputList)
    #print('Model',modelOutputList)
    
    meanMatix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    stdMatrix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    for i in range(len(testOutputList)):
        #print('Shapes', testOutputList[i].shape, modelOutputList[i].shape)
        mean, std = calculateMSE(testOutputList[i], modelOutputList[i])
        meanMatix[i,:] = mean
        stdMatrix[i,:] = std
        
    x = np.arange(len(testOutputList))
    x = x/75.
    meanHandX = meanMatix[:,0]
    meanHandY = meanMatix[:,1]
    meanHandZ = meanMatix[:,2]

    stdHandX = stdMatrix[:,0]
    stdHandY = stdMatrix[:,1]
    stdHandZ = stdMatrix[:,2]

    meanGazeX = meanMatix[:,7]
    meanGazeY = meanMatix[:,8]

    stdGazeX = stdMatrix[:,7]
    stdGazeY = stdMatrix[:,8]

    meanHeadX = meanMatix[:,9]
    meanHeadY = meanMatix[:,10]
    meanHeadZ = meanMatix[:,11]

    stdHeadX = stdMatrix[:,9]
    stdHeadY = stdMatrix[:,10]
    stdHeadZ = stdMatrix[:,11]
    
    plt.figure()
    plt.errorbar(x, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label = 'Hand X')
    plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y')
    plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z')
    plt.grid(True)
    plt.legend()
    plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Hand Position Error [m]')
    plt.savefig('HandPredictionError_'+figName+'.png')
    plt.savefig('HandPredictionError_'+figName+'.svg', format='svg', dpi=1000)

    plt.figure()
    plt.errorbar(x, meanGazeX, yerr=stdGazeX, fmt='--bo', ecolor='b', label = 'Gaze Azimuth')
    plt.errorbar(x+0.1/75., meanGazeY, yerr=stdGazeY, fmt='--ro', ecolor='r', label = 'Gaze Elevation')
    plt.grid(True)
    plt.legend()
    plt.title("Model Gaze Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Gaze Position Error [degree]')
    plt.savefig('GazePredictionError_'+figName+'.png')
    plt.savefig('GazePredictionError_'+figName+'.svg', format='svg', dpi=1000)
    
    plt.figure()
    plt.errorbar(x, meanHeadX, yerr=stdHeadX, fmt='--bo', ecolor='b', label = 'Head X')
    plt.errorbar(x+0.1/75., meanHeadY, yerr=stdHeadY, fmt='--ro', ecolor='r', label = 'Head Y')
    plt.errorbar(x+0.2/75., meanHeadZ, yerr=stdHeadZ, fmt='--go', ecolor='g', label = 'Head Z')
    plt.grid(True)
    plt.legend()
    plt.title("Model Head Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Head Position Error [m]')
    plt.savefig('HeadPredictionError_'+figName+'.png')
    plt.savefig('HeadPredictionError_'+figName+'.svg', format='svg', dpi=1000)
    pd.to_pickle(meanMatix,  r'.\Results\meanMatrix_'+figName+'.pickle')
    pd.to_pickle(stdMatrix,  r'.\Results\stdMatrix_'+figName+'.pickle')
    
def testModel(model=None, rawDataSetFileName = None, testDataSetFileName = None, data=None, epochNumber = None, noiseLevel = None, noiseIndex = None, timeStep = None):
    
    if testDataSetFileName is None:
        print('No Path Specified for Training Set!!\n ==> Quit Training\n\n')
        return None
    else:
        print('Reading Testing Data Set: ', testDataSetFileName)
        testDataSet = pd.read_pickle(testDataSetFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    #print('feature 1 ', testDataSet[0,:,5])
    [mean, std] = calculateStats(rawDataSet)
    #print('\nmean = ',mean)
    #print('\nstd = ',std)
    feat_mean = mean
    feat_std = std
    #feat_mean = np.array([ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737])
    #feat_std =  np.array([ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025])
    
    for index in range(len(feat_mean)):
        testDataSet[:,:,index] -= feat_mean[index]
        testDataSet[:,:,index] = testDataSet[:,:,index]/(feat_std[index])
    #print('feature 1 ', testDataSet[0,:,5],'\n')
   
    #testDataSet = normalizeDataSet(testDataSet, feat_mean,  feat_std)
    #testDataSet = testDataSet - np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0)
    #testDataSet = np.divide(testDataSet, np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0))
    dataSetDimension = testDataSet.shape
    print (dataSetDimension)
    
    #print (testDataSet['t1'][0].keys())
    #print (testDataSet['t1'][0].items())
    
    #training_Input = testDataSet[:,0:dataSetDimension[1],:]
    #training_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
    if timeStep == None:
        testing_Input = testDataSet[:,0:dataSetDimension[1],:]
        testing_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
        print("No Prediction Selected!")
    else:
        testing_Input = testDataSet[:,0:45,:]
        testing_Output = testDataSet[:, 45 + timeStep,0:16]
        print("TimeStep = ", str(45+timeStep), " Selected!")
    
    if (noiseLevel == 'Low'):
        noiseSTD = 0.5#0.1
    elif(noiseLevel == 'Medium'):
        noiseSTD = 2#0.8
    else:
        noiseSTD = 3#1.5
    noise = np.random.normal(0,noiseSTD,dataSetDimension[0]*45)
    noise = np.reshape(noise, (dataSetDimension[0],45))
    testing_Input[:,:,noiseIndex] = testing_Input[:,:,noiseIndex] + noise

    print('Running Model Prediction ...')
    modelOutput = model.predict(testing_Input, verbose=0)
    print(modelOutput.shape)
    print(testing_Output.shape)
    #mse = mean_squared_error(testing_Output.T, modelOutput.T, multioutput='raw_values')
    #print(mse.shape)
    #print(min(mse))
    #print(max(mse))
    #print(np.std(mse))
    figureName = 'outputFigure_45_Input_' + str(timeStep) + '_Output'
    # Un-normalizing the Data
    for index in range(modelOutput.shape[1]):
        modelOutput[:,index] = modelOutput[:,index]*(feat_std[index])
        modelOutput[:,index] += feat_mean[index]
        testing_Output[:,index] = testing_Output[:,index]*(feat_std[index])
        testing_Output[:,index] += feat_mean[index]
      
    '''
    predicted = model.predict(X_test)
    predicted = np.array(predicted)
   
    mse_predict = mean_squared_error(predicted, y_test, multioutput='raw_values')
    
    for item in history.history['loss']:
        f.write('{}\n'.format(item))

    for item in history.history['val_loss']:
        f.write('{}\n'.format(item))
    
    
    '''
    
    return model, modelOutput, testing_Output

if __name__=="__main__":
    
    meanFileName = r'.\Results\ModelOutput\41_Feat_45_Input_35_Output_50_LSTM\meanMatrix_referrence.pickle' 
    refMeanMatrix = pd.read_pickle(meanFileName)
    meanScale = np.std(refMeanMatrix, axis = 0)
    print('Scale Size', meanScale.shape)

    stdFileName = r'.\Results\ModelOutput\41_Feat_45_Input_35_Output_50_LSTM\stdMatrix_referrence.pickle' 
    refStdMatrix = pd.read_pickle(stdFileName)
    stdScale = np.std(refStdMatrix, axis = 0)
    
    print('Mean Data Size = ', refMeanMatrix.shape)
    print('Mean Data Size = ', refStdMatrix.shape)
    windowSize = [80]
    learningRateList = [0.0001]
    epochNumberList = [100]
    timeStepList = np.arange(0,35)
    noiseLevelList = ['Low', 'Medium', 'High']
    noiseIndexList = np.hstack((np.arange(0,16), np.arange(25,41)))
    print("Noise Index List = ", noiseIndexList)
    print("TimeSteps = ", timeStepList)
    columnSize = len(noiseIndexList)
    meanOutputImage = np.zeros((3,columnSize, 16))
    stdOutputImage = np.zeros((3,columnSize, 16))
    i = 0
    for noiseIndex in noiseIndexList:
        level = 0
        for noiseLevel in noiseLevelList:
            meanMatrixFileName = r'.\Results\ModelOutput\Noise Analysis\meanMatrix_'+noiseLevel+'_NoiseLevel_'+str(noiseIndex)+'_NoiseIndex0.0001_100.pickle'
            #print(meanMatrixFileName)
            modelMeanMatrix = pd.read_pickle(meanMatrixFileName)
            stdMatrixFileName = r'.\Results\ModelOutput\Noise Analysis\stdMatrix_'+noiseLevel+'_NoiseLevel_'+str(noiseIndex)+'_NoiseIndex0.0001_100.pickle'
            #print(stdMatrixFileName)
            modelStdMatrix = pd.read_pickle(stdMatrixFileName)
            print('Noise Index = ', noiseIndex, 'Noise Level = ', noiseLevel)
            meanOutputImage[level, i, :] = np.max(abs(refMeanMatrix - modelMeanMatrix), axis = 0)#, weights = (len(timeStepList) - timeStepList)/len(timeStepList))
            meanOutputImage[level, i, :] = np.divide(meanOutputImage[level, i, :], meanScale)
            
            stdOutputImage[level, i, :] = np.max(abs(refStdMatrix - modelStdMatrix), axis = 0)#, weights = (len(timeStepList) - timeStepList)/len(timeStepList))
            stdOutputImage[level, i, :] = np.divide(stdOutputImage[level, i, :], stdScale)
            level = level + 1
        i = i + 1
            
            #print(' Mean Size = ', np.shape(refMeanMatrix - modelMeanMatrix))
            #print(' STD Size = ', np.shape(refStdMatrix - modelStdMatrix))
    for i in range(3):
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(1,1,1)
        # major ticks every 20, minor ticks every 5                                      
        major_ticks = np.arange(0, 50, 2)                                              
        minor_ticks = np.arange(0, 50, 1)                                               
        
        ax.set_xticks(major_ticks)                                                       
        ax.set_xticks(minor_ticks, minor=True)                                           
        ax.set_yticks(major_ticks)                                                       
        ax.set_yticks(minor_ticks, minor=True)                                           
        
        # and a corresponding grid                                                       
        
        ax.grid(which='both')                                                            
        
        # or if you want differnet settings for the grids:                               
        ax.grid(which='minor', alpha=0.5)                                                
        ax.grid(which='major', alpha=0.9)
        plt.imshow(meanOutputImage[i,:,:], cmap="hot")
        plt.xlabel('Output Feature')
        plt.ylabel('Input Feature')
        plt.colorbar()
        plt.title('Mean of Correlation for "' + noiseLevelList[i] + '" Noise Scenario' )
        plt.savefig('mean_' + str(i) + '_Weights.png')

        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(1,1,1)
        # major ticks every 20, minor ticks every 5                                      
        major_ticks = np.arange(0, 50, 2)                                              
        minor_ticks = np.arange(0, 50, 1)                                               
        
        ax.set_xticks(major_ticks)                                                       
        ax.set_xticks(minor_ticks, minor=True)                                           
        ax.set_yticks(major_ticks)                                                       
        ax.set_yticks(minor_ticks, minor=True)                                           
        
        # and a corresponding grid                                                       
        
        ax.grid(which='both')                                                            
        
        # or if you want differnet settings for the grids:                               
        ax.grid(which='minor', alpha=0.5)                                                
        ax.grid(which='major', alpha=0.9)
        plt.imshow(stdOutputImage[i,:,:], cmap="hot")
        plt.xlabel('Output Feature')
        plt.ylabel('Input Feature')
        plt.colorbar()
        plt.title('STD of Correlation for "' + noiseLevelList[i] + '" Noise Scenario' )
        plt.savefig('std_' + str(i) + '_Weights.png')

                           #print('Testing Shape',testingOutput.shape)
                            #print('Model Shape',modelOutput.shape)
                            #plotMSE(modelOutputList, modelOutputList, timeStep)
        
                    
            #figName =  noiseLevel + '_NoiseLevel_'+str(noiseIndex)+'_NoiseIndex'+'0.0001_100'
            #plotMSE(testOutputList, modelOutputList, figName)
            
            