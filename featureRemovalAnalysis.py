'''
Created on Apr 24, 2017

@author: Kamran Binaee
'''
'''
Created on Apr 11, 2017

@author: Kamran Binaee
'''
'''
Created on Apr 9, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN
from keras.utils.np_utils import accuracy

import plotly
from plotly.graph_objs import Scatter, Layout
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from IPython.lib.tests.test_pretty import MyList

print (plotly.__version__)

plotly.offline.init_notebook_mode()

def calculateMSE(testData, modelData):
    
    print(testData.shape)
    print(modelData.shape)
    #mse = (np.sum(np.power(testData - modelData,2), axis = 0))/testData.shape[0]
    mse = (np.sum(np.abs(testData - modelData), axis = 0))/testData.shape[0]
    print('mse Shape', mse.shape)
    #print(mse)
    std = np.std(testData - modelData, axis = 0)
    print('std Shape', std.shape)
    #print(std)
    
    return mse, std
    
def plotMSE(testOutputList, modelOutputList, figName):
    
    #print('Test',testOutputList)
    #print('Model',modelOutputList)
    
    meanMatix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    stdMatrix = np.zeros((len(testOutputList), testOutputList[0].shape[1]))
    for i in range(len(testOutputList)):
        #print('Shapes', testOutputList[i].shape, modelOutputList[i].shape)
        mean, std = calculateMSE(testOutputList[i], modelOutputList[i])
        meanMatix[i,:] = mean
        stdMatrix[i,:] = std
        
    x = np.arange(len(testOutputList))
    x = x/75.
    meanHandX = meanMatix[:,0]
    meanHandY = meanMatix[:,1]
    meanHandZ = meanMatix[:,2]

    stdHandX = stdMatrix[:,0]
    stdHandY = stdMatrix[:,1]
    stdHandZ = stdMatrix[:,2]

    meanGazeX = meanMatix[:,7]
    meanGazeY = meanMatix[:,8]

    stdGazeX = stdMatrix[:,7]
    stdGazeY = stdMatrix[:,8]

    meanHeadX = meanMatix[:,9]
    meanHeadY = meanMatix[:,10]
    meanHeadZ = meanMatix[:,11]

    stdHeadX = stdMatrix[:,9]
    stdHeadY = stdMatrix[:,10]
    stdHeadZ = stdMatrix[:,11]
    
    plt.figure()
    plt.errorbar(x, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label = 'Hand X')
    plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y')
    plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z')
    plt.grid(True)
    plt.legend()
    plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Hand Position Error [m]')
    plt.savefig('HandPredictionError_'+figName+'.png')
    plt.savefig('HandPredictionError_'+figName+'.svg', format='svg', dpi=1000)

    plt.figure()
    plt.errorbar(x, meanGazeX, yerr=stdGazeX, fmt='--bo', ecolor='b', label = 'Gaze Azimuth')
    plt.errorbar(x+0.1/75., meanGazeY, yerr=stdGazeY, fmt='--ro', ecolor='r', label = 'Gaze Elevation')
    plt.grid(True)
    plt.legend()
    plt.title("Model Gaze Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Gaze Position Error [degree]')
    plt.savefig('GazePredictionError_'+figName+'.png')
    plt.savefig('GazePredictionError_'+figName+'.svg', format='svg', dpi=1000)
    
    plt.figure()
    plt.errorbar(x, meanHeadX, yerr=stdHeadX, fmt='--bo', ecolor='b', label = 'Head X')
    plt.errorbar(x+0.1/75., meanHeadY, yerr=stdHeadY, fmt='--ro', ecolor='r', label = 'Head Y')
    plt.errorbar(x+0.2/75., meanHeadZ, yerr=stdHeadZ, fmt='--go', ecolor='g', label = 'Head Z')
    plt.grid(True)
    plt.legend()
    plt.title("Model Head Prediction Error Vs. Time")
    plt.xlabel('Prediction Time [s]')
    plt.ylabel('Model Head Position Error [m]')
    plt.savefig('HeadPredictionError_'+figName+'.png')
    plt.savefig('HeadPredictionError_'+figName+'.svg', format='svg', dpi=1000)
    pd.to_pickle(meanMatix,  r'.\Results\meanMatrix_'+figName+'.pickle')
    pd.to_pickle(stdMatrix,  r'.\Results\stdMatrix_'+figName+'.pickle')

def plotOutput(testOutputList, modelOutputList, figName):
    SMALL_SIZE = 2
    MEDIUM_SIZE = 18
    BIGGER_SIZE = 18
    ERROR_BAR_LINE_SIZE = 3
    FIG_X_SIZE = 10
    FIG_Y_SIZE = 6
    x = np.arange(4000,5000)
    testData = testOutputList[0]
    modelData = modelOutputList[0]
    
    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,0], '--bo', label = 'Human Hand X')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,0],'--ro', label = 'Model Hand X')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandEliminationErrorX.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,1], '--bo', label = 'Human Hand Y')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,1],'--ro', label = 'Model Hand Y')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandEliminationErrorY.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,2], '--bo', label = 'Human Hand Z')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,2], '--ro', label = 'Model Hand Z')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandEliminationErrorZ.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,3], '--bo', label = 'Human Hand Rotation X')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,3], '--ro', label = 'Model Hand Rotation X')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandRotEliminationErrorX.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,4], '--bo', label = 'Human Hand Rotation Y')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,4], '--ro', label = 'Model Hand Rotation Y')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandRotEliminationErrorY.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,5], '--bo', label = 'Human Hand Rotation Z')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,5], '--ro', label = 'Model Hand Rotation Z')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandRotEliminationErrorZ.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,6], '--bo', label = 'Human Hand Rotation W')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,6], '--ro', label = 'Model Hand Rotation W')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HandRotEliminationErrorW.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,7], '--bo', label = 'Human Gaze Position (az)')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,7], '--ro', label = 'Model Gaze Position (az)')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('GazeEliminationErrorX.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,8], '--bo', label = 'Human Gaze Position (el)')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,8], '--ro', label = 'Model Gaze Position (el)')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('GazeEliminationErrorY.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,9], '--bo', label = 'Human Head Position X')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,9], '--ro', label = 'Model Head Position X')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HeadEliminationErrorX.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,10], '--bo', label = 'Human Head Position Y')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,10], '--ro', label = 'Model Head Position Y')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HeadEliminationErrorY.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
    plt.plot(x, testData[x,11], '--bo', label = 'Human Head Position Z')#, elinewidth = ERROR_BAR_LINE_SIZE)
    plt.plot(x, modelData[x,11], '--ro', label = 'Model Head Position Z')#, elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
    #plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
    plt.grid(True)
    plt.legend()
    #plt.title("Model Hand Prediction Error Vs. Time")
    plt.xlabel('Sample Number')
    plt.ylabel('Model Hand Position Error [m]')
    plt.legend()
     
    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.savefig('HeadEliminationErrorZ.png')
    #plt.savefig('HandEliminationError.svg', format='svg', dpi=1000)

    return
    
def testModel(model=None, rawDataSetFileName = None, testDataSetFileName = None, data=None, epochNumber = None, noiseIndex = None, timeStep = None):
    
    if testDataSetFileName is None:
        print('No Path Specified for Training Set!!\n ==> Quit Training\n\n')
        return None
    else:
        print('Reading Testing Data Set: ', testDataSetFileName)
        testDataSet = pd.read_pickle(testDataSetFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    #print('feature 1 ', testDataSet[0,:,5])
    [mean, std] = calculateStats(rawDataSet)
    #print('\nmean = ',mean)
    #print('\nstd = ',std)
    feat_mean = mean
    feat_std = std
    #feat_mean = np.array([ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737])
    #feat_std =  np.array([ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025])
    
    for index in range(len(feat_mean)):
        testDataSet[:,:,index] -= feat_mean[index]
        testDataSet[:,:,index] = testDataSet[:,:,index]/(feat_std[index])
    #print('feature 1 ', testDataSet[0,:,5],'\n')
   
    #testDataSet = normalizeDataSet(testDataSet, feat_mean,  feat_std)
    #testDataSet = testDataSet - np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0)
    #testDataSet = np.divide(testDataSet, np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0))
    dataSetDimension = testDataSet.shape
    print (dataSetDimension)
    
    #print (testDataSet['t1'][0].keys())
    #print (testDataSet['t1'][0].items())
    
    #training_Input = testDataSet[:,0:dataSetDimension[1],:]
    #training_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
    if timeStep == None:
        testing_Input = testDataSet[:,0:dataSetDimension[1],:]
        testing_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
        print("No Prediction Selected!")
    else:
        testing_Input = testDataSet[:,0:45,:]
        testing_Output = testDataSet[:, 45 + timeStep,0:16]
        print("TimeStep = ", str(45+timeStep), " Selected!")
    
    testing_Input[:,:,noiseIndex] = 0

    print('Running Model Prediction ...')
    modelOutput = model.predict(testing_Input, verbose=0)
    print(modelOutput.shape)
    print(testing_Output.shape)
    #mse = mean_squared_error(testing_Output.T, modelOutput.T, multioutput='raw_values')
    #print(mse.shape)
    #print(min(mse))
    #print(max(mse))
    #print(np.std(mse))
    figureName = 'outputFigure_45_Input_' + str(timeStep) + '_Output'
    # Un-normalizing the Data
    for index in range(modelOutput.shape[1]):
        modelOutput[:,index] = modelOutput[:,index]*(feat_std[index])
        modelOutput[:,index] += feat_mean[index]
        testing_Output[:,index] = testing_Output[:,index]*(feat_std[index])
        testing_Output[:,index] += feat_mean[index]
      
    '''
    predicted = model.predict(X_test)
    predicted = np.array(predicted)
   
    mse_predict = mean_squared_error(predicted, y_test, multioutput='raw_values')
    
    for item in history.history['loss']:
        f.write('{}\n'.format(item))

    for item in history.history['val_loss']:
        f.write('{}\n'.format(item))
    
    
    '''
    
    return model, modelOutput, testing_Output

if __name__=="__main__":
    rawDataSetFileName = r'.\Datasets\Success\DataSet_Formatted_Success.pickle'
    #dataSetFileName = None
    trialTypeFileName = r'.\Datasets\Success\trialType_Success.pickle'
    
    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_6.pickle'
    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_6.pickle'
    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_6.pickle'
    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_6.pickle'
    
    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_21.pickle'
    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_21.pickle'
    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_21.pickle'
    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_21.pickle'
    
    #inputSequenceList = [3, 6, 9, 12, 15, 18, 21]
    #windowSize = [15, 20, 25, 30, 35, 40, 45]#, 50, 55, 60, 65, 70, 75]#, 80]
    windowSize = [80]
    learningRateList = [0.0001]
    epochNumberList = [100]
    timeStepList = np.arange(0,1)
    noiseLevelList = ['Low', 'Medium', 'High']
    noiseIndexList = np.arange(25,26)
    print("Noise Index List = ", noiseIndexList)
    print("TimeSteps = ", timeStepList)
    
    for noiseIndex in noiseIndexList:
        modelOutputList = []
        testOutputList = []
        for timeStep in timeStepList:
            for epochNumber in epochNumberList:
                for learningRate in learningRateList:
                    for inputSeqLength in windowSize:
                        print('\n\nTraining on Input Sequence Length = ', inputSeqLength)
                        print('Learning Rate = ', learningRate)
                        print('Epoch Number = ', epochNumber)
                        trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_'+str(inputSeqLength)+'.pickle'
                        testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_'+str(inputSeqLength)+'.pickle'
                        trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_'+str(inputSeqLength)+'.pickle'
                        trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_'+str(inputSeqLength)+'.pickle'
                        weights_path = r'.\Results\Weights\41_Feat_45_Input_35_Output_50_LSTM\weights_' + str(inputSeqLength - 35) +'_' + str(timeStep)+ '_0.0001_100'
                        model = createRNN(inputSeqLength - 35, weights_path, learningRate )
                        print('RNN-LSTM Model Created ...\n')
                        model, modelOutput, testingOutput = testModel(model, rawDataSetFileName, testDataSetFileName, epochNumber = epochNumber, noiseIndex = noiseIndex, timeStep=timeStep)
                        pd.to_pickle(modelOutput,  r'.\Results\modelOutput_' + str(inputSeqLength - 35) + '_Input_' + str(timeStep) +'_Output_'+
                                     str(noiseIndex)+'_FeatureIndex'+'0.0001_100.pickle')
                        modelOutputList.append(modelOutput)
                        testOutputList.append(testingOutput)
                        #print('Testing Shape',testingOutput.shape)
                        #print('Model Shape',modelOutput.shape)
                        #plotMSE(modelOutputList, modelOutputList, timeStep)
    
                
        figName =  str(noiseIndex)+'_FeatureIndex'+'0.0001_100'
        #plotMSE(testOutputList, modelOutputList, figName)
        print('My Data = ',len(testOutputList), len(modelOutputList), testOutputList[0].shape, modelOutputList[0].shape)
        plotOutput(testOutputList, modelOutputList, figName)