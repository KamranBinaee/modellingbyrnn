'''
Created on May 3, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN
from keras.utils.np_utils import accuracy

import plotly
from plotly.graph_objs import Scatter, Layout
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from IPython.lib.tests.test_pretty import MyList
import re

mainDataSet = pd.read_pickle(r'.\Datasets\Success\DataSet_Success.pickle')
#trialType = pd.read_pickle(r'.\Datasets\Success\trialType_Success.pickle')

keys = list(mainDataSet.keys())
print(keys)

length = []
for key,value in mainDataSet.items():
     
    myList = re.split('_',key)
    trialType = myList[1]
    if trialType=='t9':
        #print("{}={}:shape{}".format(trtype,trial,mainDataSet[trial].shape))
        length.append(mainDataSet[key].shape[0])
        print(mainDataSet[key].shape[0])
        #print('\n\n')
#     
plt.hist(length, bins=np.arange(150))
plt.show()

# 
# meanMatrix = pd.read_pickle(r'.\Results\ModelOutput\41_Feat_45_Input_35_Output_50_LSTM\meanMatrix_referrence.pickle')
# stdMatrix = pd.read_pickle(r'.\Results\ModelOutput\41_Feat_45_Input_35_Output_50_LSTM\stdMatrix_referrence.pickle')
# 
# x = np.arange(35)
# x = x/75.
# meanHandX = meanMatrix[:,0]
# meanHandY = meanMatrix[:,1]
# meanHandZ = meanMatrix[:,2]
# 
# stdHandX = stdMatrix[:,0]
# stdHandY = stdMatrix[:,1]
# stdHandZ = stdMatrix[:,2]
# 
# meanGazeX = meanMatrix[:,7]
# meanGazeY = meanMatrix[:,8]
# 
# stdGazeX = stdMatrix[:,7]
# stdGazeY = stdMatrix[:,8]
# 
# meanHeadX = meanMatrix[:,9]
# meanHeadY = meanMatrix[:,10]
# meanHeadZ = meanMatrix[:,11]
# 
# stdHeadX = stdMatrix[:,9]
# stdHeadY = stdMatrix[:,10]
# stdHeadZ = stdMatrix[:,11]
# 
# SMALL_SIZE = 2
# MEDIUM_SIZE = 18
# BIGGER_SIZE = 18
# ERROR_BAR_LINE_SIZE = 3
# FIG_X_SIZE = 10
# FIG_Y_SIZE = 6
# 
# plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
# plt.errorbar(x, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label = 'Hand X', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.errorbar(x+0.1/75., meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label = 'Hand Y', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.errorbar(x+0.2/75., meanHandZ, yerr=stdHandZ, fmt='--go', ecolor='g', label = 'Hand Z', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.grid(True)
# plt.legend()
# #plt.title("Model Hand Prediction Error Vs. Time")
# plt.xlabel('Prediction Time [s]')
# plt.ylabel('Model Hand Position Error [m]')
# 
# plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
# plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
# plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
# plt.savefig('HandPredictionError.png')
# plt.savefig('HandPredictionError.svg', format='svg', dpi=1000)
# 
# plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
# plt.errorbar(x, meanGazeX, yerr=stdGazeX, fmt='--bo', ecolor='b', label = 'Gaze Azimuth', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.errorbar(x+0.1/75., meanGazeY, yerr=stdGazeY, fmt='--ro', ecolor='r', label = 'Gaze Elevation', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.grid(True)
# plt.legend()
# #plt.title("Model Gaze Prediction Error Vs. Time")
# plt.xlabel('Prediction Time [s]')
# plt.ylabel('Model Gaze Position Error [degree]')
# 
# plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
# plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
# plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
# plt.savefig('GazePredictionError.png')
# plt.savefig('GazePredictionError.svg', format='svg', dpi=1000)
# 
# plt.figure(figsize=(FIG_X_SIZE, FIG_Y_SIZE))
# plt.errorbar(x, meanHeadX, yerr=stdHeadX, fmt='--bo', ecolor='b', label = 'Head X', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.errorbar(x+0.1/75., meanHeadY, yerr=stdHeadY, fmt='--ro', ecolor='r', label = 'Head Y', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.errorbar(x+0.2/75., meanHeadZ, yerr=stdHeadZ, fmt='--go', ecolor='g', label = 'Head Z', elinewidth = ERROR_BAR_LINE_SIZE)
# plt.grid(True)
# plt.legend()
# #plt.title("Model Head Prediction Error Vs. Time")
# plt.xlabel('Prediction Time [s]')
# plt.ylabel('Model Head Position Error [m]')
# plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
# plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
# plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
# plt.savefig('HeadPredictionError.png')
# plt.savefig('HeadPredictionError.svg', format='svg', dpi=1000)
