'''
Created on Feb 5, 2017

@author: Kamran Binaee
'''
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
import math
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD, RMSprop, Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch


def SaveValuesIntoNpArray(x, fname):
    """Save matrix of image into numpy array. x - image matrix, fname - path/name of future file without extension"""
    fname = fname + '.npy'
    np.save(fname, x, allow_pickle=True, fix_imports=True)


def createRNN(input_seq_length, weights_path=None, learningRate=0.001):
    """LSTM model that has 3 LSTM layers with 50 units each and one Dense layer on top.
    Model can predict one time step vector with 5 values;
    The model is tuned for optimizer: optimizer = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    """
    # input_seq_length = 40
    model = Sequential()
    # layers = [41, 20, 20, 16]
    layers = [41, 50,  16]
    print('Creating RNN with Layer Dimensions of :', layers)
    # first layer
    model.add(LSTM(
        input_dim=layers[0],
        output_dim=layers[1], input_length=input_seq_length,
        return_sequences=False, unroll=True))
    '''
    model.add(Dropout(0.2))
    # second layer
    model.add(LSTM(
           layers[2],
           return_sequences=True, unroll=True))

    model.add(Dropout(0.2))
    # third layer
    model.add(LSTM(
           layers[2],
           return_sequences=False, unroll=True))
    model.add(Dropout(0.2))
    '''
    # Dense layer
    model.add(Dense(
        output_dim=layers[2]))  # Changed from 2
    model.add(Activation("linear"))
    # Original Values
    optimizer = Adam(lr=learningRate, beta_1=0.9,
                     beta_2=0.999, epsilon=1e-08, decay=0.0)
    # optimizer = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.01)
    # rmsprop = RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
    model.compile(loss="mse", optimizer=optimizer)
    print('model compiled...')
    if weights_path:
        model.load_weights(weights_path)
    return model


def calculateStats(dataSet):

    trialList = list(dataSet.keys())
    print(trialList)
    print(dataSet[trialList[1]])
    allTrials = np.zeros((1, 41))
    count = 0
    for trialNumber in trialList[1:]:
        tempVar = dataSet[trialNumber]
        allTrials = np.vstack((allTrials, tempVar))
        count = count + dataSet[trialNumber].shape[0]
    allTrials = np.delete(allTrials, 0, 0)
    print(count)
    print(allTrials.shape)
    mean = np.mean(allTrials, axis=0)
    std = np.std(allTrials, axis=0)
    #print('Mean=\n', mean)
    #print('STD=\n', std)
    return [mean, std]


def normalizeDataSet(dataSet, mean, std):
    trialList = list(dataSet.keys())

    count = 0
    normalizedDataSet = dict()
    for trialNumber in trialList:
        normalizedDataSet[trialNumber] = dataSet[trialNumber] - \
            np.repeat(mean, dataSet[trialNumber].shape[0], axis=0)
        normalizedDataSet[trialNumber] = np.divide(
            normalizedDataSet[trialNumber], np.repeat(std, dataSet[trialNumber].shape[0], axis=0))

    print(dataSet[trialNumber][0:3, :])
    print(normalizedDataSet[trialNumber][0:3, :])
    return normalizedDataSet


def PlotLoss(fname_Loss, fname_valLoss, figureName):
    """function for loss plot
    fname_Loss - full path to training loss
    fname_valLoss - full path to training loss
    """
    loss = pd.read_pickle(fname_Loss)
    valloss = pd.read_pickle(fname_valLoss)
    plt.figure()
    plt.plot(np.arange(len(loss)) + 1, loss,
             linewidth=4, label='Learning Loss')
    plt.plot(np.arange(len(valloss)) + 1, valloss,
             linewidth=4, label='Validation Loss')
    plt.grid(True)
    plt.legend()
    plt.xlabel('Epoch Number')
    plt.ylabel('Loss')
    plt.savefig(str(figureName) + '.png')
    # plt.show()


def trainModel(myDummyInputSize, model=None, rawDataSetFileName=None, trainDataSetFileName=None, epochNumber=None, learningRate=None, inputSeqLength=None, timeStep=None):

    if trainDataSetFileName is None:
        print('No Path Specified for Training Set!!\n ==> Quit Training\n\n')
        return None
    else:
        print('Reading Training Data Set: ', trainDataSetFileName)
        trainDataSet = pd.read_pickle(trainDataSetFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    # print('feature 1 ', trainDataSet[0,:,5])
    [mean, std] = calculateStats(rawDataSet)
    print('\nmean Before = ', mean)
    print('\nstd Before= ', std)
    feat_mean = mean
    feat_std = std
    # feat_mean = np.array([ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737])
    # feat_std =  np.array([ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025])

    for index in range(len(feat_mean)):
        trainDataSet[:, :, index] -= feat_mean[index]
        trainDataSet[:, :, index] = trainDataSet[:,
                                                 :, index] / (feat_std[index])
    #[mean, std] = calculateStats(trainDataSet)
    # print('\nmean After = ',mean)
    # print('\nstd After = ',std)

    # print('feature 1 ', trainDataSet[0,:,5],'\n')

    # trainDataSet = normalizeDataSet(trainDataSet, feat_mean,  feat_std)
    # trainDataSet = trainDataSet - np.repeat(feat_mean, trainDataSet.shape[0], trainDataSet.shape[1] , axis=0)
    # trainDataSet = np.divide(trainDataSet, np.repeat(feat_mean, trainDataSet.shape[0], trainDataSet.shape[1] , axis=0))
    dataSetDimension = trainDataSet.shape
    print(dataSetDimension)

    # myDummyInputSize = 5
    # print (trainDataSet['t1'][0].keys())
    # print (trainDataSet['t1'][0].items())
    if timeStep == None:
        training_Input = trainDataSet[:, 0:dataSetDimension[1] - 1, :]
        training_Output = trainDataSet[:, dataSetDimension[1] - 1, 0:16]
        print("No Prediction Selected!")
    else:

        training_Input = trainDataSet[:, 0:myDummyInputSize, :]
        training_Output = trainDataSet[:, myDummyInputSize + timeStep, 0:16]
        print("TimeStep = ", str(myDummyInputSize + timeStep), " Selected!")

    # training_Input = trainDataSet[:,0:dataSetDimension[1],16:41]
    # training_Output = trainDataSet[:,dataSetDimension[1] - 1,0:16]

    # X_train, y_train, X_test, y_test = data
    cb = EarlyStopping(monitor='val_loss', patience=7, verbose=1, mode='min')
    history = model.fit(training_Input, training_Output, batch_size=128,
                        nb_epoch=epochNumber, callbacks=[cb], validation_split=0.15, shuffle=True)
    # fname_weights = './Old_data_varing_in_seq_50_50_50/weights_50_50_50/wieghts_rnn505050_inseq{}'.format(nb_input_seq)
    # fname_weights = r'.\Results\Weights\weights_'+str(dataSetDimension[1] - 35) + '_' + str(timeStep) + '_' + str(learningRate) + '_' + str(epochNumber)
    fname_weights = r'.\Results\Weights\weights_' + \
        str(myDummyInputSize) + '_' + str(timeStep) + \
        '_' + str(learningRate) + '_' + str(epochNumber)
    model.save_weights(fname_weights)
    # fname_Loss = r'.\Results\Weights\Loss_'+str(dataSetDimension[1] - 35)+ '_' + str(timeStep) + '_' + str(learningRate) + '_' + str(epochNumber)
    fname_Loss = r'.\Results\Weights\Loss_' + str(myDummyInputSize) + '_' + str(
        timeStep) + '_' + str(learningRate) + '_' + str(epochNumber)
    pd.to_pickle(history.history['loss'], fname_Loss + r'.pickle')
    # fname_Loss = r'.\Results\Weights\ValidationLoss_'+str(dataSetDimension[1] - 35) + '_' + str(timeStep) + '_' + str(learningRate) + '_' + str(epochNumber)
    fname_Loss = r'.\Results\Weights\ValidationLoss_' + \
        str(myDummyInputSize) + '_' + str(timeStep) + \
        '_' + str(learningRate) + '_' + str(epochNumber)
    pd.to_pickle(history.history['val_loss'], fname_Loss + r'.pickle')

    '''
    predicted = model.predict(X_test)
    predicted = np.array(predicted)

    mse_predict = mean_squared_error(
        predicted, y_test, multioutput='raw_values')

    for item in history.history['loss']:
        f.write('{}\n'.format(item))

    for item in history.history['val_loss']:
        f.write('{}\n'.format(item))


    '''

    return model, history


if __name__ == "__main__":

    trainOnWhat = 'All'  # , 'Success', 'Fail'
    subjectID = '2016-5-3-12-52'
    subjectID = '2016-5-5-13-7'
    rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All-2016-5-5-13-7.pickle'
    rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All-2016-5-5-13-7.pickle'
    trainDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All-2016-5-5-13-7.pickle'

    # if (trainOnWhat == 'Success'):
    #     rawDataSetFileName = r'.\Datasets\Success\DataSet_Formatted_Success.pickle'
    #     #dataSetFileName = None
    #     trialTypeFileName = r'.\Datasets\Success\trialType_Success.pickle'

    #     trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_21.pickle'
    #     testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_21.pickle'
    #     trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_21.pickle'
    #     trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_21.pickle'
    # elif(trainOnWhat == 'Fail'):
    #     rawDataSetFileName = r'.\Datasets\Fail\DataSet_Formatted_Fail.pickle'
    #     #dataSetFileName = None
    #     trialTypeFileName = r'.\Datasets\Fail\trialType_Fail.pickle'

    #     trainDataSetFileName = r'.\Datasets\Fail\Train_DataSet_Fail_21.pickle'
    #     testDataSetFileName = r'.\Datasets\Fail\Test_DataSet_Fail_21.pickle'
    #     trialTypeFileName = r'.\Datasets\Fail\Test_TrialType_Fail_21.pickle'
    #     trialNumberFileName = r'.\Datasets\Fail\Test_TrialNumber_Fail_21.pickle'
    # elif(trainOnWhat == 'Fail'):
    #     rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
    #     trainDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
    '''
    inputSequenceList = [3, 6, 9, 12, 15, 18, 21]
    inputSeqLength = 0
    # myList = list(trialType.keys())
    # print (sorted(myList))

    # for inputSeqLength in inputSequenceList:
    resizeDataSet(dataSetFileName, trialTypeFileName, inputSeqLength)

    '''

    # windowSize = [15, 20, 25, 30, 35, 40, 45]#, 50, 55, 60, 65, 70, 75]#, 80]
    windowSize = [80]
    learningRateList = [0.0001]
    epochNumberList = [500]  # [100]
    timeStepList = [1]  # [5, 15, 30]#np.arange(0,35)
    print("TimeSteps = ", timeStepList)
    for timeStep in timeStepList:
        for epochNumber in epochNumberList:
            for learningRate in learningRateList:
                for inputSeqLength in windowSize:
                    print('\n\nTraining on Input Sequence Length = ',
                          inputSeqLength)
                    print('Learning Rate = ', learningRate)
                    print('Epoch Number = ', epochNumber)
                    if (trainOnWhat == 'Success'):
                        trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_' + \
                            str(inputSeqLength) + '.pickle'
                    elif(trainOnWhat == 'Fail'):
                        trainDataSetFileName = r'.\Datasets\Fail\Train_DataSet_Fail_' + \
                            str(inputSeqLength) + '.pickle'
                    elif(trainOnWhat == 'All'):
                        rawDataSetFileName = r'.\Datasets\2016-5-5-13-7\DataSet_All.pickle'
                        trainDataSetFileName = r'.\Datasets\2016-5-5-13-7\Train_DataSet_80.pickle'

                    model = createRNN(inputSeqLength - 35, None, learningRate)
                    # model = createRNN(mydummySize, None, learningRate)
                    print('RNN-LSTM Model Created ...\n')
                    model, history = trainModel(inputSeqLength - 35, model, rawDataSetFileName, trainDataSetFileName,
                                                epochNumber=epochNumber, learningRate=learningRate, inputSeqLength=inputSeqLength, timeStep=timeStep)

            # fname_Loss = r'.\Results\Weights\My Backup\Loss_{}_0001_100.pickle'.format(inputSeqLength)
            # print(fname_Loss)
            # fname_valLoss = r'.\Results\Weights\My Backup\ValidationLoss_{}_0001_100.pickle'.format(inputSeqLength)
            # figureName = 'LossPlot_' + str(inputSeqLength) + '_' + str(learningRate) + '_' + str(epochNumber)
            # PlotLoss(fname_Loss, fname_valLoss, figureName)
