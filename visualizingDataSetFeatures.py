'''
Created on Jan 19, 2018

@author: Kamran Binaee
'''


import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
#from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD, RMSprop, Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis


pickleFileName = r'.\Datasets\2016-5-5-13-7\exp_data-2016-5-5-13-7.pickle'

print('Reading the All Subject Pickle File ... \n ', pickleFileName)
eyeToScreenDistance = 0.0725
ballDiameter = 0.01
df = pd.read_pickle(pickleFileName)
print('Raw Dataset Read in!')
rawDataFrame = df['raw']
processedDataFrame = df['processed']
calibDataFrame = df['calibration']
trialInfoDataFrame = df['trialInfo']

# processedDataFrame.

processedDataFrame['ballSize'] = (
    180 / np.pi) * np.arctan(ballDiameter * 0.5 / processedDataFrame['ballDepth'])
ballSize = processedDataFrame['ballSize']
ballSize_shifted = np.roll(ballSize, -1, axis=0)

timeArray = rawDataFrame.frameTime.values
timeArray_shifted = np.roll(timeArray, -1)
timeDiff = timeArray_shifted - timeArray

processedDataFrame['ballExpansionRate'] = np.divide(
    ballSize_shifted - ballSize, timeDiff)
